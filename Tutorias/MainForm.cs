﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 24/11/2018
 * Time: 21:05
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Tutorias
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public Usuario Usuarios {
			get {
				return usuarios;
			}
		}

		private Usuario usuarios = new Usuario();
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//Conexion a la db
		MySqlConnection Conectar (){
			MySqlConnection conexion = new MySqlConnection();
			
			conexion.ConnectionString="Server=localhost; password=; database=tutoria; User Id=root;";
			
			return conexion;
		}
				
		void BtnEntrarClick(object sender, EventArgs e) //Intenta poner una contraseña aquí abajo, para que sea unica en root xd
		{												//Sin ingresarlo a otra tabla alv, por lo menos funciona :p
			if (txtUsuario.Text.Equals("root") && txtContrasena.Text.Equals("232323")){
				Menu menu = new Menu();
				menu.Show();	
			}else if (ConfirmarUsuario() && Usuarios.Tipo.Equals("1")){
				menuProfesor menuProfe = new menuProfesor(Usuarios);
				menuProfe.Show();
			}else if (ConfirmarUsuario() && Usuarios.Tipo.Equals("2")){
				menuEstudiante menuAlumno = new menuEstudiante (Usuarios);
				menuAlumno.Show();
			}else{
				MessageBox.Show("Contraseña o usuario incorrectos");
			}
		}
		
		public bool ConfirmarUsuario(){
			String contrasena = txtContrasena.Text;
			String usuario = txtUsuario.Text;		
			
			MySqlConnection conexion = Conectar();
			
			try{
				
				String query = "SELECT * FROM usuario WHERE CodigoUsuario="+usuario+" AND contrasena='"+contrasena+"'";
				MySqlCommand comando = new MySqlCommand (query,conexion);
				conexion.Open();
				
				MySqlDataReader rd = comando.ExecuteReader();
				
				while(rd.Read()){
					if (Convert.ToString(rd["CodigoUsuario"]).Equals(usuario) && Convert.ToString(rd["contrasena"]).Equals(contrasena)){
						usuarios.Codigo = Convert.ToString(rd["CodigoUsuario"]);
						usuarios.Tipo = Convert.ToString(rd["idTipo"]);
						conexion.Close();
						return true;
					}
				}
				
				conexion.Close();
				
			}catch(MySqlException){
				MessageBox.Show("error");
			}
				
			return false;
		}
		void BtnEntrarProfesorClick(object sender, EventArgs e)
		{	
			
		}
	}
}
