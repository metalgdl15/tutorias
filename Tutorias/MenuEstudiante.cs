﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 03/12/2018
 * Time: 20:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace Tutorias
{
	/// <summary>
	/// Description of menuEstudiante.
	/// </summary>
	public partial class menuEstudiante : Form
	{
		
		private Usuario user = new Usuario();

		public Usuario User {
			get {
				return user;
			}
			set {
				user = value;
			}
		}

		public menuEstudiante(Usuario usu)
		{
			User = usu;
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//Conectarse a la DB
		MySqlConnection Conectar (){
			MySqlConnection conexion = new MySqlConnection();
			
			conexion.ConnectionString="Server=localhost; password=; database=tutoria; User Id=root;";
			
			return conexion;
		}
		
		void MenuEstudianteLoad(object sender, EventArgs e)
		{
			agregarPeriodos();
			agregarUsuario();
		}
		
		public void agregarUsuario(){
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT CONCAT(apellidoP,' ',apellidoM, ' ', nombre) nom FROM estudiante WHERE Codigo="+User.Codigo;
				MySqlCommand comando = new MySqlCommand(query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
				
				while(rd.Read()){
					lblUsuarioE.Text = Convert.ToString(rd["nom"]);
				}
				conexion.Close();
				
			} catch (MySqlException) {
				
				MessageBox.Show("error");
			}
		}
		
		//Inicializa Periodos
		public void agregarPeriodos(){
			
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT idPeriodo, CONCAT(inicio,'   <->   ',fin) as ciclo FROM periodo";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxPeriodoTutoria.ValueMember ="idPeriodo";
				boxPeriodoTutoria.DisplayMember="ciclo";
				boxPeriodoTutoria.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		void BtnConsultarMateriasClick(object sender, EventArgs e)
		{
			tablaAsignaturaAlumno.Rows.Clear();
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "SELECT CONCAT(p.aPaterno,' ',p.aMaterno,' ',p.nombre) as profe, a.nombre as materia, CONCAT(grup.Nombre,' ', gra.Nombre) as grup " +
						"FROM (profesor_asignatura pa, grupo_grado g) " +
						"INNER JOIN profesor p ON p.Codigo = pa.idProfesor " +
						"INNER JOIN asignatura a ON a.idAsignatura = pa.idAsignatura " +
						"INNER JOIN grupo grup ON g.idGrupo = grup.idGrupo AND pa.idGrupo_grado = g.id " +
						"INNER JOIN grado gra ON gra.idGrado = g.idGrado " +
						"INNER JOIN estudiante e ON e.idGrupo_grado=g.id WHERE e.Codigo="+User.Codigo;
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaAsignaturaAlumno.Rows.Add();
						
						tablaAsignaturaAlumno.Rows[renglon].Cells["grupoProfesor"].Value=
							rd.GetString(rd.GetOrdinal("profe"));
						tablaAsignaturaAlumno.Rows[renglon].Cells["grupoAsignatura"].Value=
							rd.GetString(rd.GetOrdinal("materia"));
						tablaAsignaturaAlumno.Rows[renglon].Cells["gradoGrupo"].Value=
							rd.GetString(rd.GetOrdinal("grup"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
		}
		void BtnConsultarTutoriaClick(object sender, EventArgs e)
		{
			String idPeriodo = boxPeriodoTutoria.SelectedValue.ToString();
			lblObjetivo.Text ="";
			lblMeta.Text = "";
			lblAcuerdo.Text ="";
			
			if (idPeriodo == ""){
				MessageBox.Show("Ingrese un periodo");
			}else{
			
				MySqlConnection conexion = Conectar();
				//try {
					String query = "SELECT CONCAT(p.aPaterno,' ',p.aMaterno,' ',p.nombre)as profe, b.objetivo, b.acuerdo, b.meta FROM tutoria_individual t " +
						"INNER JOIN bitacora b ON b.idTutoria = t.idTutoria " +
						"INNER JOIN profesor p ON t.idProfesor= p.Codigo " +
						"WHERE t.idPeriodo="+idPeriodo+" AND t.idEstudiante="+User.Codigo;
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
					
					while (rd.Read()) {						
						lblProfesorT.Text=Convert.ToString(rd["profe"]);
						lblObjetivo.Text= Convert.ToString(rd["objetivo"]);
						lblMeta.Text= Convert.ToString(rd["meta"]);
						lblAcuerdo.Text = Convert.ToString(rd["acuerdo"]);
					}
					
					conexion.Close();
			//	} catch (MySqlException) {
			//		MessageBox.Show("error");
			//	}
			}
	
		}

	}
}
