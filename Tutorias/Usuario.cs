﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 03/12/2018
 * Time: 11:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Tutorias
{
	/// <summary>
	/// Description of Usuario.
	/// </summary>
	public class Usuario
	{
		
		private String codigo;
		private String tipo;
		
		public Usuario()
		{
		}

		public String Codigo {
			get {
				return codigo;
			}
			set {
				codigo = value;
			}
		}

		public String Tipo {
			get {
				return tipo;
			}
			set {
				tipo = value;
			}
		}
	}
}
