﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 01/12/2018
 * Time: 13:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Tutorias
{
	partial class menuProfesor
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TabControl tabMenuProfesor;
		private System.Windows.Forms.TabPage tabMateriaProfesor;
		private System.Windows.Forms.TabPage tabTutoriasProfesor;
		private System.Windows.Forms.DataGridView tablaClaseProfesor;
		private System.Windows.Forms.Button btnConsultarClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn profesorClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn materiaClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn grupoClase;
		private System.Windows.Forms.Button btnMostrarTutoria;
		private System.Windows.Forms.Button btnAgregearTutoriaInd;
		private System.Windows.Forms.DataGridView tablaTutoria;
		private System.Windows.Forms.Label lblPeriodoTutoria;
		private System.Windows.Forms.Label lblAlumnoTutoria;
		private System.Windows.Forms.ComboBox boxPeriodoIndividual;
		private System.Windows.Forms.ComboBox boxAlumnoIndividual;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoTutoria;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tutor;
		private System.Windows.Forms.DataGridViewTextBoxColumn Estudiante;
		private System.Windows.Forms.DataGridViewTextBoxColumn Periodo;
		private System.Windows.Forms.Button btnAddBitacora;
		private System.Windows.Forms.TextBox txtTutoriaBusqueda;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblBitacora;
		private System.Windows.Forms.TextBox txtObjetivo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtAcuerdo;
		private System.Windows.Forms.TextBox txtMeta;
		private System.Windows.Forms.Button Mostrar;
		private System.Windows.Forms.Label lblMaterias;
		private System.Windows.Forms.Label lblUsuario;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menuProfesor));
			this.tabMenuProfesor = new System.Windows.Forms.TabControl();
			this.tabMateriaProfesor = new System.Windows.Forms.TabPage();
			this.lblMaterias = new System.Windows.Forms.Label();
			this.btnConsultarClase = new System.Windows.Forms.Button();
			this.tablaClaseProfesor = new System.Windows.Forms.DataGridView();
			this.codigoClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.profesorClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.materiaClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grupoClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabTutoriasProfesor = new System.Windows.Forms.TabPage();
			this.Mostrar = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtAcuerdo = new System.Windows.Forms.TextBox();
			this.txtMeta = new System.Windows.Forms.TextBox();
			this.btnAddBitacora = new System.Windows.Forms.Button();
			this.txtTutoriaBusqueda = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lblBitacora = new System.Windows.Forms.Label();
			this.txtObjetivo = new System.Windows.Forms.TextBox();
			this.btnMostrarTutoria = new System.Windows.Forms.Button();
			this.btnAgregearTutoriaInd = new System.Windows.Forms.Button();
			this.tablaTutoria = new System.Windows.Forms.DataGridView();
			this.codigoTutoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Tutor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Estudiante = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Periodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lblPeriodoTutoria = new System.Windows.Forms.Label();
			this.lblAlumnoTutoria = new System.Windows.Forms.Label();
			this.boxPeriodoIndividual = new System.Windows.Forms.ComboBox();
			this.boxAlumnoIndividual = new System.Windows.Forms.ComboBox();
			this.lblUsuario = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button1 = new System.Windows.Forms.Button();
			this.tabMenuProfesor.SuspendLayout();
			this.tabMateriaProfesor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaClaseProfesor)).BeginInit();
			this.tabTutoriasProfesor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaTutoria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// tabMenuProfesor
			// 
			this.tabMenuProfesor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tabMenuProfesor.Controls.Add(this.tabMateriaProfesor);
			this.tabMenuProfesor.Controls.Add(this.tabTutoriasProfesor);
			this.tabMenuProfesor.Location = new System.Drawing.Point(0, 24);
			this.tabMenuProfesor.Name = "tabMenuProfesor";
			this.tabMenuProfesor.SelectedIndex = 0;
			this.tabMenuProfesor.Size = new System.Drawing.Size(767, 512);
			this.tabMenuProfesor.TabIndex = 0;
			// 
			// tabMateriaProfesor
			// 
			this.tabMateriaProfesor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabMateriaProfesor.BackgroundImage")));
			this.tabMateriaProfesor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabMateriaProfesor.Controls.Add(this.lblMaterias);
			this.tabMateriaProfesor.Controls.Add(this.btnConsultarClase);
			this.tabMateriaProfesor.Controls.Add(this.tablaClaseProfesor);
			this.tabMateriaProfesor.Location = new System.Drawing.Point(4, 22);
			this.tabMateriaProfesor.Name = "tabMateriaProfesor";
			this.tabMateriaProfesor.Padding = new System.Windows.Forms.Padding(3);
			this.tabMateriaProfesor.Size = new System.Drawing.Size(759, 486);
			this.tabMateriaProfesor.TabIndex = 0;
			this.tabMateriaProfesor.Text = "Materia";
			this.tabMateriaProfesor.UseVisualStyleBackColor = true;
			// 
			// lblMaterias
			// 
			this.lblMaterias.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMaterias.Location = new System.Drawing.Point(207, 40);
			this.lblMaterias.Name = "lblMaterias";
			this.lblMaterias.Size = new System.Drawing.Size(310, 36);
			this.lblMaterias.TabIndex = 2;
			this.lblMaterias.Text = "Listado de materias asignadas";
			// 
			// btnConsultarClase
			// 
			this.btnConsultarClase.Location = new System.Drawing.Point(332, 310);
			this.btnConsultarClase.Name = "btnConsultarClase";
			this.btnConsultarClase.Size = new System.Drawing.Size(75, 23);
			this.btnConsultarClase.TabIndex = 1;
			this.btnConsultarClase.Text = "Consultar";
			this.btnConsultarClase.UseVisualStyleBackColor = true;
			this.btnConsultarClase.Click += new System.EventHandler(this.BtnConsultarClaseClick);
			// 
			// tablaClaseProfesor
			// 
			this.tablaClaseProfesor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaClaseProfesor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaClaseProfesor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoClase,
			this.profesorClase,
			this.materiaClase,
			this.grupoClase});
			this.tablaClaseProfesor.GridColor = System.Drawing.SystemColors.ControlDarkDark;
			this.tablaClaseProfesor.Location = new System.Drawing.Point(103, 91);
			this.tablaClaseProfesor.Name = "tablaClaseProfesor";
			this.tablaClaseProfesor.Size = new System.Drawing.Size(524, 193);
			this.tablaClaseProfesor.TabIndex = 0;
			// 
			// codigoClase
			// 
			this.codigoClase.HeaderText = "Codigo de la clase";
			this.codigoClase.Name = "codigoClase";
			// 
			// profesorClase
			// 
			this.profesorClase.HeaderText = "Maestro";
			this.profesorClase.Name = "profesorClase";
			// 
			// materiaClase
			// 
			this.materiaClase.HeaderText = "Materia";
			this.materiaClase.Name = "materiaClase";
			// 
			// grupoClase
			// 
			this.grupoClase.HeaderText = "Grupo";
			this.grupoClase.Name = "grupoClase";
			// 
			// tabTutoriasProfesor
			// 
			this.tabTutoriasProfesor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabTutoriasProfesor.BackgroundImage")));
			this.tabTutoriasProfesor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabTutoriasProfesor.Controls.Add(this.button1);
			this.tabTutoriasProfesor.Controls.Add(this.Mostrar);
			this.tabTutoriasProfesor.Controls.Add(this.label3);
			this.tabTutoriasProfesor.Controls.Add(this.label2);
			this.tabTutoriasProfesor.Controls.Add(this.txtAcuerdo);
			this.tabTutoriasProfesor.Controls.Add(this.txtMeta);
			this.tabTutoriasProfesor.Controls.Add(this.btnAddBitacora);
			this.tabTutoriasProfesor.Controls.Add(this.txtTutoriaBusqueda);
			this.tabTutoriasProfesor.Controls.Add(this.label1);
			this.tabTutoriasProfesor.Controls.Add(this.lblBitacora);
			this.tabTutoriasProfesor.Controls.Add(this.txtObjetivo);
			this.tabTutoriasProfesor.Controls.Add(this.btnMostrarTutoria);
			this.tabTutoriasProfesor.Controls.Add(this.btnAgregearTutoriaInd);
			this.tabTutoriasProfesor.Controls.Add(this.tablaTutoria);
			this.tabTutoriasProfesor.Controls.Add(this.lblPeriodoTutoria);
			this.tabTutoriasProfesor.Controls.Add(this.lblAlumnoTutoria);
			this.tabTutoriasProfesor.Controls.Add(this.boxPeriodoIndividual);
			this.tabTutoriasProfesor.Controls.Add(this.boxAlumnoIndividual);
			this.tabTutoriasProfesor.Location = new System.Drawing.Point(4, 22);
			this.tabTutoriasProfesor.Name = "tabTutoriasProfesor";
			this.tabTutoriasProfesor.Padding = new System.Windows.Forms.Padding(3);
			this.tabTutoriasProfesor.Size = new System.Drawing.Size(759, 486);
			this.tabTutoriasProfesor.TabIndex = 1;
			this.tabTutoriasProfesor.Text = "Tutorias";
			this.tabTutoriasProfesor.UseVisualStyleBackColor = true;
			// 
			// Mostrar
			// 
			this.Mostrar.Location = new System.Drawing.Point(463, 243);
			this.Mostrar.Name = "Mostrar";
			this.Mostrar.Size = new System.Drawing.Size(99, 23);
			this.Mostrar.TabIndex = 19;
			this.Mostrar.Text = "Mostrar bitacora";
			this.Mostrar.UseVisualStyleBackColor = true;
			this.Mostrar.Click += new System.EventHandler(this.MostrarClick);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(557, 279);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(81, 18);
			this.label3.TabIndex = 18;
			this.label3.Text = "Acuerdo";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(337, 279);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(81, 18);
			this.label2.TabIndex = 17;
			this.label2.Text = "Meta";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtAcuerdo
			// 
			this.txtAcuerdo.Location = new System.Drawing.Point(499, 304);
			this.txtAcuerdo.Multiline = true;
			this.txtAcuerdo.Name = "txtAcuerdo";
			this.txtAcuerdo.Size = new System.Drawing.Size(183, 126);
			this.txtAcuerdo.TabIndex = 16;
			// 
			// txtMeta
			// 
			this.txtMeta.Location = new System.Drawing.Point(291, 300);
			this.txtMeta.Multiline = true;
			this.txtMeta.Name = "txtMeta";
			this.txtMeta.Size = new System.Drawing.Size(183, 126);
			this.txtMeta.TabIndex = 15;
			// 
			// btnAddBitacora
			// 
			this.btnAddBitacora.Location = new System.Drawing.Point(337, 243);
			this.btnAddBitacora.Name = "btnAddBitacora";
			this.btnAddBitacora.Size = new System.Drawing.Size(110, 23);
			this.btnAddBitacora.TabIndex = 14;
			this.btnAddBitacora.Text = "Insertar bitacora";
			this.btnAddBitacora.UseVisualStyleBackColor = true;
			this.btnAddBitacora.Click += new System.EventHandler(this.BtnAddBitacoraClick);
			// 
			// txtTutoriaBusqueda
			// 
			this.txtTutoriaBusqueda.Location = new System.Drawing.Point(193, 243);
			this.txtTutoriaBusqueda.Name = "txtTutoriaBusqueda";
			this.txtTutoriaBusqueda.Size = new System.Drawing.Size(125, 20);
			this.txtTutoriaBusqueda.TabIndex = 13;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(73, 246);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(114, 23);
			this.label1.TabIndex = 12;
			this.label1.Text = "Numero de la tutoria";
			// 
			// lblBitacora
			// 
			this.lblBitacora.Location = new System.Drawing.Point(99, 279);
			this.lblBitacora.Name = "lblBitacora";
			this.lblBitacora.Size = new System.Drawing.Size(81, 18);
			this.lblBitacora.TabIndex = 11;
			this.lblBitacora.Text = "Objetivo";
			this.lblBitacora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtObjetivo
			// 
			this.txtObjetivo.Location = new System.Drawing.Point(73, 300);
			this.txtObjetivo.Multiline = true;
			this.txtObjetivo.Name = "txtObjetivo";
			this.txtObjetivo.Size = new System.Drawing.Size(183, 126);
			this.txtObjetivo.TabIndex = 10;
			// 
			// btnMostrarTutoria
			// 
			this.btnMostrarTutoria.Location = new System.Drawing.Point(524, 88);
			this.btnMostrarTutoria.Name = "btnMostrarTutoria";
			this.btnMostrarTutoria.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarTutoria.TabIndex = 8;
			this.btnMostrarTutoria.Text = "Mostrar";
			this.btnMostrarTutoria.UseVisualStyleBackColor = true;
			this.btnMostrarTutoria.Click += new System.EventHandler(this.BtnMostrarTutoriaClick);
			// 
			// btnAgregearTutoriaInd
			// 
			this.btnAgregearTutoriaInd.Location = new System.Drawing.Point(146, 88);
			this.btnAgregearTutoriaInd.Name = "btnAgregearTutoriaInd";
			this.btnAgregearTutoriaInd.Size = new System.Drawing.Size(75, 23);
			this.btnAgregearTutoriaInd.TabIndex = 7;
			this.btnAgregearTutoriaInd.Text = "Agregar";
			this.btnAgregearTutoriaInd.UseVisualStyleBackColor = true;
			this.btnAgregearTutoriaInd.Click += new System.EventHandler(this.BtnAgregearTutoriaIndClick);
			// 
			// tablaTutoria
			// 
			this.tablaTutoria.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaTutoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaTutoria.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoTutoria,
			this.Tutor,
			this.Estudiante,
			this.Periodo});
			this.tablaTutoria.Location = new System.Drawing.Point(73, 130);
			this.tablaTutoria.Name = "tablaTutoria";
			this.tablaTutoria.Size = new System.Drawing.Size(609, 96);
			this.tablaTutoria.TabIndex = 6;
			// 
			// codigoTutoria
			// 
			this.codigoTutoria.HeaderText = "No Tutoria";
			this.codigoTutoria.Name = "codigoTutoria";
			// 
			// Tutor
			// 
			this.Tutor.HeaderText = "Tutor";
			this.Tutor.Name = "Tutor";
			// 
			// Estudiante
			// 
			this.Estudiante.HeaderText = "Estudiante";
			this.Estudiante.Name = "Estudiante";
			// 
			// Periodo
			// 
			this.Periodo.HeaderText = "Periodo";
			this.Periodo.Name = "Periodo";
			// 
			// lblPeriodoTutoria
			// 
			this.lblPeriodoTutoria.Location = new System.Drawing.Point(499, 15);
			this.lblPeriodoTutoria.Name = "lblPeriodoTutoria";
			this.lblPeriodoTutoria.Size = new System.Drawing.Size(100, 23);
			this.lblPeriodoTutoria.TabIndex = 5;
			this.lblPeriodoTutoria.Text = "Periodo";
			this.lblPeriodoTutoria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblAlumnoTutoria
			// 
			this.lblAlumnoTutoria.Location = new System.Drawing.Point(172, 15);
			this.lblAlumnoTutoria.Name = "lblAlumnoTutoria";
			this.lblAlumnoTutoria.Size = new System.Drawing.Size(100, 23);
			this.lblAlumnoTutoria.TabIndex = 4;
			this.lblAlumnoTutoria.Text = "Alumno";
			this.lblAlumnoTutoria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// boxPeriodoIndividual
			// 
			this.boxPeriodoIndividual.FormattingEnabled = true;
			this.boxPeriodoIndividual.Location = new System.Drawing.Point(477, 41);
			this.boxPeriodoIndividual.Name = "boxPeriodoIndividual";
			this.boxPeriodoIndividual.Size = new System.Drawing.Size(161, 21);
			this.boxPeriodoIndividual.TabIndex = 2;
			// 
			// boxAlumnoIndividual
			// 
			this.boxAlumnoIndividual.FormattingEnabled = true;
			this.boxAlumnoIndividual.Location = new System.Drawing.Point(120, 41);
			this.boxAlumnoIndividual.Name = "boxAlumnoIndividual";
			this.boxAlumnoIndividual.Size = new System.Drawing.Size(173, 21);
			this.boxAlumnoIndividual.TabIndex = 1;
			// 
			// lblUsuario
			// 
			this.lblUsuario.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUsuario.Location = new System.Drawing.Point(419, 6);
			this.lblUsuario.Name = "lblUsuario";
			this.lblUsuario.Size = new System.Drawing.Size(252, 25);
			this.lblUsuario.TabIndex = 1;
			this.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(677, 2);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(62, 34);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(577, 243);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(105, 23);
			this.button1.TabIndex = 20;
			this.button1.Text = "Actualizar bitacora";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// menuProfesor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(772, 514);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.lblUsuario);
			this.Controls.Add(this.tabMenuProfesor);
			this.Name = "menuProfesor";
			this.Text = "menuProfesor";
			this.Load += new System.EventHandler(this.MenuProfesorLoad);
			this.tabMenuProfesor.ResumeLayout(false);
			this.tabMateriaProfesor.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tablaClaseProfesor)).EndInit();
			this.tabTutoriasProfesor.ResumeLayout(false);
			this.tabTutoriasProfesor.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaTutoria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}
	}
}
