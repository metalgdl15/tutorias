﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 03/12/2018
 * Time: 20:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Tutorias
{
	partial class menuEstudiante
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TabControl tabAlumnos;
		private System.Windows.Forms.TabPage tabClasesAlumno;
		private System.Windows.Forms.TabPage tabTutoriasAlumno;
		private System.Windows.Forms.Button btnConsultarMaterias;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView tablaAsignaturaAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn gradoGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn grupoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn grupoAsignatura;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblAcuerdo;
		private System.Windows.Forms.Label lblMeta;
		private System.Windows.Forms.Label lblObjetivo;
		private System.Windows.Forms.ComboBox boxPeriodoTutoria;
		private System.Windows.Forms.Button btnConsultarTutoria;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblProfesorT;
		private System.Windows.Forms.Label lblUsuarioE;
		private System.Windows.Forms.PictureBox pictureBox1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menuEstudiante));
			this.tabAlumnos = new System.Windows.Forms.TabControl();
			this.tabClasesAlumno = new System.Windows.Forms.TabPage();
			this.btnConsultarMaterias = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tablaAsignaturaAlumno = new System.Windows.Forms.DataGridView();
			this.gradoGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grupoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grupoAsignatura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabTutoriasAlumno = new System.Windows.Forms.TabPage();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lblProfesorT = new System.Windows.Forms.Label();
			this.boxPeriodoTutoria = new System.Windows.Forms.ComboBox();
			this.btnConsultarTutoria = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lblAcuerdo = new System.Windows.Forms.Label();
			this.lblMeta = new System.Windows.Forms.Label();
			this.lblObjetivo = new System.Windows.Forms.Label();
			this.lblUsuarioE = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tabAlumnos.SuspendLayout();
			this.tabClasesAlumno.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaAsignaturaAlumno)).BeginInit();
			this.tabTutoriasAlumno.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// tabAlumnos
			// 
			this.tabAlumnos.Controls.Add(this.tabClasesAlumno);
			this.tabAlumnos.Controls.Add(this.tabTutoriasAlumno);
			this.tabAlumnos.Location = new System.Drawing.Point(1, 29);
			this.tabAlumnos.Name = "tabAlumnos";
			this.tabAlumnos.SelectedIndex = 0;
			this.tabAlumnos.Size = new System.Drawing.Size(711, 375);
			this.tabAlumnos.TabIndex = 0;
			// 
			// tabClasesAlumno
			// 
			this.tabClasesAlumno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabClasesAlumno.BackgroundImage")));
			this.tabClasesAlumno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabClasesAlumno.Controls.Add(this.btnConsultarMaterias);
			this.tabClasesAlumno.Controls.Add(this.label1);
			this.tabClasesAlumno.Controls.Add(this.tablaAsignaturaAlumno);
			this.tabClasesAlumno.Location = new System.Drawing.Point(4, 22);
			this.tabClasesAlumno.Name = "tabClasesAlumno";
			this.tabClasesAlumno.Padding = new System.Windows.Forms.Padding(3);
			this.tabClasesAlumno.Size = new System.Drawing.Size(703, 349);
			this.tabClasesAlumno.TabIndex = 0;
			this.tabClasesAlumno.Text = "Clases";
			this.tabClasesAlumno.UseVisualStyleBackColor = true;
			// 
			// btnConsultarMaterias
			// 
			this.btnConsultarMaterias.Location = new System.Drawing.Point(267, 279);
			this.btnConsultarMaterias.Name = "btnConsultarMaterias";
			this.btnConsultarMaterias.Size = new System.Drawing.Size(94, 23);
			this.btnConsultarMaterias.TabIndex = 2;
			this.btnConsultarMaterias.Text = "Consultar";
			this.btnConsultarMaterias.UseVisualStyleBackColor = true;
			this.btnConsultarMaterias.Click += new System.EventHandler(this.BtnConsultarMateriasClick);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(230, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(234, 38);
			this.label1.TabIndex = 1;
			this.label1.Text = "Listado de Materias";
			// 
			// tablaAsignaturaAlumno
			// 
			this.tablaAsignaturaAlumno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tablaAsignaturaAlumno.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaAsignaturaAlumno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaAsignaturaAlumno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.gradoGrupo,
			this.grupoProfesor,
			this.grupoAsignatura});
			this.tablaAsignaturaAlumno.Location = new System.Drawing.Point(101, 81);
			this.tablaAsignaturaAlumno.Name = "tablaAsignaturaAlumno";
			this.tablaAsignaturaAlumno.Size = new System.Drawing.Size(464, 166);
			this.tablaAsignaturaAlumno.TabIndex = 0;
			// 
			// gradoGrupo
			// 
			this.gradoGrupo.HeaderText = "Grupo - Grado";
			this.gradoGrupo.Name = "gradoGrupo";
			// 
			// grupoProfesor
			// 
			this.grupoProfesor.HeaderText = "Profesor";
			this.grupoProfesor.Name = "grupoProfesor";
			// 
			// grupoAsignatura
			// 
			this.grupoAsignatura.HeaderText = "Asignatura";
			this.grupoAsignatura.Name = "grupoAsignatura";
			// 
			// tabTutoriasAlumno
			// 
			this.tabTutoriasAlumno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabTutoriasAlumno.BackgroundImage")));
			this.tabTutoriasAlumno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabTutoriasAlumno.Controls.Add(this.label3);
			this.tabTutoriasAlumno.Controls.Add(this.label2);
			this.tabTutoriasAlumno.Controls.Add(this.lblProfesorT);
			this.tabTutoriasAlumno.Controls.Add(this.boxPeriodoTutoria);
			this.tabTutoriasAlumno.Controls.Add(this.btnConsultarTutoria);
			this.tabTutoriasAlumno.Controls.Add(this.label7);
			this.tabTutoriasAlumno.Controls.Add(this.label6);
			this.tabTutoriasAlumno.Controls.Add(this.label5);
			this.tabTutoriasAlumno.Controls.Add(this.lblAcuerdo);
			this.tabTutoriasAlumno.Controls.Add(this.lblMeta);
			this.tabTutoriasAlumno.Controls.Add(this.lblObjetivo);
			this.tabTutoriasAlumno.Location = new System.Drawing.Point(4, 22);
			this.tabTutoriasAlumno.Name = "tabTutoriasAlumno";
			this.tabTutoriasAlumno.Padding = new System.Windows.Forms.Padding(3);
			this.tabTutoriasAlumno.Size = new System.Drawing.Size(703, 349);
			this.tabTutoriasAlumno.TabIndex = 1;
			this.tabTutoriasAlumno.Text = "Tutorias";
			this.tabTutoriasAlumno.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(45, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(155, 23);
			this.label3.TabIndex = 10;
			this.label3.Text = "Maestro asignado";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(313, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 9;
			this.label2.Text = "Periodo";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblProfesorT
			// 
			this.lblProfesorT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblProfesorT.Location = new System.Drawing.Point(25, 68);
			this.lblProfesorT.Name = "lblProfesorT";
			this.lblProfesorT.Size = new System.Drawing.Size(196, 23);
			this.lblProfesorT.TabIndex = 8;
			// 
			// boxPeriodoTutoria
			// 
			this.boxPeriodoTutoria.FormattingEnabled = true;
			this.boxPeriodoTutoria.Location = new System.Drawing.Point(261, 70);
			this.boxPeriodoTutoria.Name = "boxPeriodoTutoria";
			this.boxPeriodoTutoria.Size = new System.Drawing.Size(184, 21);
			this.boxPeriodoTutoria.TabIndex = 7;
			// 
			// btnConsultarTutoria
			// 
			this.btnConsultarTutoria.Location = new System.Drawing.Point(562, 68);
			this.btnConsultarTutoria.Name = "btnConsultarTutoria";
			this.btnConsultarTutoria.Size = new System.Drawing.Size(75, 23);
			this.btnConsultarTutoria.TabIndex = 6;
			this.btnConsultarTutoria.Text = "Consultar";
			this.btnConsultarTutoria.UseVisualStyleBackColor = true;
			this.btnConsultarTutoria.Click += new System.EventHandler(this.BtnConsultarTutoriaClick);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(515, 123);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(122, 37);
			this.label7.TabIndex = 5;
			this.label7.Text = "Acuerdo";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(291, 123);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(122, 37);
			this.label6.TabIndex = 4;
			this.label6.Text = "Meta";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(57, 123);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(122, 37);
			this.label5.TabIndex = 3;
			this.label5.Text = "Objetivo";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblAcuerdo
			// 
			this.lblAcuerdo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblAcuerdo.Location = new System.Drawing.Point(475, 160);
			this.lblAcuerdo.Name = "lblAcuerdo";
			this.lblAcuerdo.Size = new System.Drawing.Size(196, 161);
			this.lblAcuerdo.TabIndex = 2;
			// 
			// lblMeta
			// 
			this.lblMeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblMeta.Location = new System.Drawing.Point(249, 160);
			this.lblMeta.Name = "lblMeta";
			this.lblMeta.Size = new System.Drawing.Size(196, 161);
			this.lblMeta.TabIndex = 1;
			// 
			// lblObjetivo
			// 
			this.lblObjetivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblObjetivo.Location = new System.Drawing.Point(25, 160);
			this.lblObjetivo.Name = "lblObjetivo";
			this.lblObjetivo.Size = new System.Drawing.Size(196, 161);
			this.lblObjetivo.TabIndex = 0;
			// 
			// lblUsuarioE
			// 
			this.lblUsuarioE.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUsuarioE.Location = new System.Drawing.Point(372, 9);
			this.lblUsuarioE.Name = "lblUsuarioE";
			this.lblUsuarioE.Size = new System.Drawing.Size(216, 23);
			this.lblUsuarioE.TabIndex = 1;
			this.lblUsuarioE.Text = "label4";
			this.lblUsuarioE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(619, 9);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(54, 36);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// menuEstudiante
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(715, 406);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.lblUsuarioE);
			this.Controls.Add(this.tabAlumnos);
			this.Name = "menuEstudiante";
			this.Text = "menuEstudiante";
			this.Load += new System.EventHandler(this.MenuEstudianteLoad);
			this.tabAlumnos.ResumeLayout(false);
			this.tabClasesAlumno.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tablaAsignaturaAlumno)).EndInit();
			this.tabTutoriasAlumno.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}
	}
}
