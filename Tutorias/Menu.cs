﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 25/11/2018
 * Time: 18:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace Tutorias
{
	/// <summary>
	/// Description of Menu.
	/// </summary>
	public partial class Menu : Form
	{
		public Menu()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//Conectarse a la DB
		MySqlConnection Conectar (){
			MySqlConnection conexion = new MySqlConnection();
			
			conexion.ConnectionString="Server=localhost; password=; database=tutoria; User Id=root;";
			
			return conexion;
		}
		
		//Inicializar todo dentro del meni menu
		void MenuLoad(object sender, EventArgs e)
		{
			agregarCarreras();
			agregarGrados();
			agregarGrupos();
			agregarProfesores();
			agregarAsignatura();
			agregarAlumnos();
		}
		
		//inicializar Grupos en Alumnos
		public void agregarGrupos(){
			
			boxGrupoClases.DataSource = null;
			comboBoxAlumnoGrupo.DataSource = null;
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT gg.id, CONCAT (grupo.nombre, '  ', grado.nombre ) as gru_gra FROM grupo_grado gg " +
				" INNER JOIN grupo ON gg.idGrupo = grupo.idGrupo" + 
				" INNER JOIN grado ON gg.idGrado = grado.idGrado";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				comboBoxAlumnoGrupo.ValueMember ="id";
				comboBoxAlumnoGrupo.DisplayMember="gru_gra";
				comboBoxAlumnoGrupo.DataSource=dt2;
				
				boxGrupoClases.ValueMember="id";
				boxGrupoClases.DisplayMember="gru_gra";
				boxGrupoClases.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
			
		}
		
		//inicializa alumnos
		public void agregarAlumnos(){
			boxEstudianteUsuario.DataSource=null;
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT Codigo, CONCAT(apellidoP,' ',apellidoP,' ',nombre) as alumno FROM estudiante";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxEstudianteUsuario.ValueMember ="Codigo";
				boxEstudianteUsuario.DisplayMember="alumno";
				boxEstudianteUsuario.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		//incializar carreras en profesores, grupos
		public void agregarCarreras(){
			boxCarreraGrupo.DataSource= null;
			boxCarreraAsignatura.DataSource = null;
			boxCarreraProfesor.DataSource = null;
			
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT Codigo, Nombre FROM carrera";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da = new MySqlDataAdapter(comando);
				DataTable dt = new DataTable();
				
				dt.Rows.InsertAt(dt.NewRow(), 0);
				
				da.Fill(dt);
				
				boxCarreraProfesor.ValueMember ="Codigo";
				boxCarreraProfesor.DisplayMember="Nombre";
				boxCarreraProfesor.DataSource=dt;
				
				boxCarreraAsignatura.ValueMember="Codigo";
				boxCarreraAsignatura.DisplayMember="Nombre";
				boxCarreraAsignatura.DataSource=dt;
				
				boxCarreraGrupo.ValueMember ="Codigo";
				boxCarreraGrupo.DisplayMember="Nombre";
				boxCarreraGrupo.DataSource=dt;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		//inicializar grado en grupos;
		public void agregarGrados(){
			boxGradoAsignatura.DataSource = null;
			
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT * FROM grado";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxGradoAsignatura.ValueMember ="idGrado";
				boxGradoAsignatura.DisplayMember="nombre";
				boxGradoAsignatura.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		//inicializar profesores en clases, usuario
		public void agregarProfesores(){
			
			boxProfesorClases.DataSource=null;
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT Codigo, CONCAT(aPaterno,' ',aMaterno,' ',nombre) as profe FROM profesor";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxProfesorClases.ValueMember ="Codigo";
				boxProfesorClases.DisplayMember="profe";
				boxProfesorClases.DataSource=dt2;
				
				boxMaestroUsuario.ValueMember ="Codigo";
				boxMaestroUsuario.DisplayMember="profe";
				boxMaestroUsuario.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}	 
		
		//Agregar Asignatura en clases
		public void agregarAsignatura(){
			boxAsignaturaClases.DataSource=null;
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT * FROM asignatura";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxAsignaturaClases.ValueMember ="idAsignatura";
				boxAsignaturaClases.DisplayMember="nombre";
				boxAsignaturaClases.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		//Agregar Carrera
		void BtnAddCarreraClick(object sender, EventArgs e)
		{
			String nombre=txtNombreCarrera.Text;
			String nivel = txtNivelCarrera.Text;
			String area = txtAreaCarrera.Text;
			
			if(nombre == "" || nivel == "" || area ==""){
				MessageBox.Show("llene todos los campos");
			}
			else{
				MySqlConnection conexion= Conectar();
				
				try {
					String query="INSERT INTO carrera (nombre, nivel, area) VALUES ('"+nombre+"',"+nivel+",'"+area+"')";
					MySqlCommand comando = new MySqlCommand (query, conexion);
					conexion.Open();
					
					comando.ExecuteNonQuery();
					
					conexion.Close();
					MessageBox.Show("Se ha agregado la carrera");
					mostrarCarreras();
					
					agregarCarreras();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}	
		}
		
		//mostrar todas las carreras
		public void mostrarCarreras(){
			tablaCarreras.Rows.Clear();
			MySqlConnection conexion = Conectar();
			try {
				String query = "SELECT * FROM carrera";
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
				
				while (rd.Read()) {
					int renglon = tablaCarreras.Rows.Add();
					tablaCarreras.Rows[renglon].Cells["Codigo"].Value=
						rd.GetInt32(rd.GetOrdinal("Codigo")).ToString();
					tablaCarreras.Rows[renglon].Cells["Nombre"].Value=
						rd.GetString(rd.GetOrdinal("Nombre"));
					tablaCarreras.Rows[renglon].Cells["Nivel"].Value=
						rd.GetInt32(rd.GetOrdinal("Nivel")).ToString();
					tablaCarreras.Rows[renglon].Cells["Area"].Value=
						rd.GetString(rd.GetOrdinal("Area"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//boton Mostrar
		void BtnViewCarreraClick(object sender, EventArgs e)
		{
			mostrarCarreras();
		}
		
		//buscar carrera
		void BtnSearchCarreraClick(object sender, EventArgs e)
		{
			string codigo = txtSearchCarrera.Text;
			tablaCarreras.Rows.Clear();
			MySqlConnection conexion = Conectar();
			try {
				String query = "SELECT * FROM carrera WHERE codigo ="+codigo;
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
				
				while (rd.Read()) {
					int renglon = tablaCarreras.Rows.Add();
					tablaCarreras.Rows[renglon].Cells["Codigo"].Value=
						rd.GetInt32(rd.GetOrdinal("Codigo")).ToString();
					tablaCarreras.Rows[renglon].Cells["Nombre"].Value=
						rd.GetString(rd.GetOrdinal("Nombre"));
					tablaCarreras.Rows[renglon].Cells["Nivel"].Value=
						rd.GetInt32(rd.GetOrdinal("Nivel")).ToString();
					tablaCarreras.Rows[renglon].Cells["Area"].Value=
						rd.GetString(rd.GetOrdinal("Area"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Eliminar carrera
		void BtnEliminarCarreraClick(object sender, EventArgs e)
		{
			string id = txtSearchCarrera.Text;
			
			if (id == ""){
				MessageBox.Show("Igrese el codigo de la carrera");
			}
			
			else{
				MySqlConnection conexion = Conectar();
			
				String query="DELETE FROM carrera WHERE codigo="+id;
			
				DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
			
				if(dialogo == DialogResult.Yes){
					try {
						MySqlCommand comando= new MySqlCommand(query,conexion);
			
						conexion.Open();
						
						comando.ExecuteNonQuery();
						
						conexion.Close();	
						
						MessageBox.Show("Se ha eliminado");
						
					} catch (MySqlException) {
						MessageBox.Show(" ¡ ¡ ¡ Error ! ! !");
					}
					
				}
			}
		}
		
		//Actualizar carrera
		void BtnUpdateCarreraClick(object sender, EventArgs e)
		{
			String id = txtSearchCarrera.Text;
			String nombre = txtNombreCarrera.Text;
			String area = txtAreaCarrera.Text;
			String nivel = txtNivelCarrera.Text;
			
			if (id == ""){
				MessageBox.Show("Igrese el codigo de la carrera");
				
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					if (nombre != ""){
						query = "UPDATE carrera SET nombre='"+ nombre+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (area != ""){
						query = "UPDATE carrera SET area='"+ area+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (nivel != ""){
						query = "UPDATE carrera SET nivel='"+nivel+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					boxCarreraProfesor.DataSource = null;
					agregarCarreras();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
		}
		
		//Agregar Periodos
		void BtnAgregarPeriodoClick(object sender, EventArgs e)
		{
			String nombre = txtNombrePeriodo.Text;
			String inicio = dateInicioPeriodo.Text;
			String fin = dateFinPeriodo.Text;
			
			if (nombre == "" || !dateInicioPeriodo.Checked || !dateFinPeriodo.Checked){
				MessageBox.Show("Llene todos los campos");
			}
			else{
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "INSERT INTO periodo (inicio, fin,nombre) " +
						"VALUES ('"+inicio+"','"+fin+"','"+nombre+"')";
					
					MySqlCommand comando =new MySqlCommand(query, conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha agregado");
					MostrarPeriodos();
				} catch (MySqlException) {
					MessageBox.Show("ERROR");
				}
			}
			
		}
		
		//Mostrar Periodos
		public void MostrarPeriodos(){
			tablaPeriodos.Rows.Clear();
			MySqlConnection conexion = Conectar();
			try {
				String query = "SELECT * FROM periodo";
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
				
				while (rd.Read()) {
					int renglon = tablaPeriodos.Rows.Add();
					tablaPeriodos.Rows[renglon].Cells["idPeriodo"].Value=
						rd.GetInt32(rd.GetOrdinal("idPeriodo")).ToString();
					tablaPeriodos.Rows[renglon].Cells["fechaInicio"].Value=
						rd.GetDateTime(rd.GetOrdinal("inicio")).ToString("yyyy/MM/dd");
					tablaPeriodos.Rows[renglon].Cells["fechaFin"].Value=
						rd.GetDateTime(rd.GetOrdinal("fin")).ToString("yyyy/MM/dd");
					tablaPeriodos.Rows[renglon].Cells["nombrePeriodo"].Value=
						rd.GetString(rd.GetOrdinal("nombre"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Boton mostrar Periodos
		void BtnMostrarPeriodoClick(object sender, EventArgs e)
		{
			MostrarPeriodos();
		}
		
		//Eliminar Periodos
		void BtnEliminarPeriodoClick(object sender, EventArgs e)
		{
			String id = txtIdPeriodo.Text;
			if (id == ""){
				MessageBox.Show("ingrese el id del periodo");
			}
			else{
				MySqlConnection conexion = Conectar();
				DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				if(dialogo == DialogResult.Yes){
					try {
						String query ="DELETE FROM periodo WHERE idPeriodo="+id;
						MySqlCommand comando = new MySqlCommand(query, conexion);
						conexion.Open();
						comando.ExecuteNonQuery();
						conexion.Close();
						MessageBox.Show("Se ha eliminado");
					}catch (MySqlException) {
						MessageBox.Show("Error");
					}
				}
			}
		}
		
		//Actualizar Periodo
		void BtnActualizarPeriodoClick(object sender, EventArgs e)
		{
			String id = txtIdPeriodo.Text;
			String nombre = txtNombrePeriodo.Text;
			String inicio = dateInicioPeriodo.Text;
			String fin = dateFinPeriodo.Text;
			
			if (id == ""){
				MessageBox.Show("Igrese el codigo de la carrera");
				
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					if (nombre != ""){
						query = "UPDATE periodo SET nombre='"+ nombre+"' WHERE idPeriodo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (dateInicioPeriodo.Checked){
						query = "UPDATE periodo SET inicio='"+ inicio+"' WHERE idPeriodo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (dateFinPeriodo.Checked){
						query = "UPDATE periodo SET fin='"+fin+"' WHERE idPeriodo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
		}
		
		//Agregar profesores
		void BtnAgregarProfesorClick(object sender, EventArgs e)
		{
			String nombre = txtNombreProfesor.Text;
			String codigo = txtCodigoProfesor.Text;
			String telefono = txtTelProfesor.Text;
			String correo = txtMail.Text;
			String carrera = boxCarreraProfesor.SelectedValue.ToString();
			String aPaterno = txtAPprofesor.Text;
			String aMaterno = txtAMprofesor.Text;
			
			MessageBox.Show(carrera);
			
			if (nombre == "" || codigo == "" || telefono =="" || correo == "" || aPaterno == "" || aMaterno =="" || carrera==""){
				MessageBox.Show("Llene todos los campos");
			}
			else{
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "INSERT INTO profesor (Codigo, Nombre, aPaterno, aMaterno, Telefono, Correo, idCarrera) " +
						"VALUES ("+codigo+",'"+nombre+"','"+aPaterno+"','"+aMaterno+"','"+telefono+"','"+correo+"',"+carrera+")";
					
					MySqlCommand comando =new MySqlCommand(query, conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha agregado");
					MostrarPeriodos();
					
					agregarProfesores();
				} catch (MySqlException) {
					MessageBox.Show("ERROR");
				}
			}
	
		}
		
		//botron para mostrar profesores
		void BtnMostrarProfesoresClick(object sender, EventArgs e)
		{
			MostrarProfesores();
		}
		
		//Mostrar profesores
		public void MostrarProfesores(){
			tablaProfesores.Visible = true;
			tablaProfesores.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT p.Codigo,p.Nombre,p.aPaterno,p.aMaterno,p.Correo,p.Telefono,c.Nombre as caNom " +
					"FROM profesor p LEFT JOIN carrera c ON c.Codigo=p.idCarrera";
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
			
				while (rd.Read()) {
					int renglon = tablaProfesores.Rows.Add();
					tablaProfesores.Rows[renglon].Cells["codigoProfesor"].Value=
						rd.GetInt32(rd.GetOrdinal("Codigo")).ToString();
					tablaProfesores.Rows[renglon].Cells["nombreProfesor"].Value=
						rd.GetString(rd.GetOrdinal("Nombre"));
					tablaProfesores.Rows[renglon].Cells["aPaternoProfesor"].Value=
						rd.GetString(rd.GetOrdinal("aPaterno")).ToString();
					tablaProfesores.Rows[renglon].Cells["aMaternoProfesor"].Value=
						rd.GetString(rd.GetOrdinal("aMaterno"));
					tablaProfesores.Rows[renglon].Cells["correoProfesor"].Value=
						rd.GetString(rd.GetOrdinal("Correo"));
					tablaProfesores.Rows[renglon].Cells["telefonoProfesor"].Value=
						rd.GetString(rd.GetOrdinal("Telefono"));
					tablaProfesores.Rows[renglon].Cells["carreraProfesor"].Value=
						rd.GetString(rd.GetOrdinal("caNom"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		
		//Eliminar profesor
		void BtnEliminarProfesorClick(object sender, EventArgs e)
		{
			String codigo = txtBuscarProfesor.Text;
			MySqlConnection conexion = Conectar();
			
			if (codigo ==""){
				MessageBox.Show("Ingrese el codigo para eliminar");
			}else{
				try{
					DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				
					String query ="DELETE FROM profesor WHERE codigo="+codigo;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					if(dialogo == DialogResult.Yes){
						comando.ExecuteNonQuery();
						MessageBox.Show("Se ha eliminado");
					}else{
						MessageBox.Show("Se ha cancelado");
					}
					conexion.Close();
				}catch(MySqlException){
					MessageBox.Show("Error");
				}
			}
		}
		
		//BuscarProfesor
		void BtnBuscarProfesorClick(object sender, EventArgs e)
		{
			tablaProfesores.Visible = true;
			tablaProfesores.Rows.Clear();
			
			String id = txtBuscarProfesor.Text;
			
			if (id==""){
				MessageBox.Show("ingrese el codigo para buscar");
			}
			else{
				
				
				MySqlConnection conexion = Conectar();
				try {
					String query = "SELECT p.Codigo,p.Nombre,p.aPaterno,p.aMaterno,p.Correo,p.Telefono,c.Nombre as caNom " +
						"FROM profesor p LEFT JOIN carrera c ON p.idCarrera=c.Codigo WHERE p.Codigo="+id;
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaProfesores.Rows.Add();
						tablaProfesores.Rows[renglon].Cells["codigoProfesor"].Value=
							rd.GetInt32(rd.GetOrdinal("Codigo")).ToString();
						tablaProfesores.Rows[renglon].Cells["nombreProfesor"].Value=
							rd.GetString(rd.GetOrdinal("Nombre"));
						tablaProfesores.Rows[renglon].Cells["aPaternoProfesor"].Value=
							rd.GetString(rd.GetOrdinal("aPaterno")).ToString();
						tablaProfesores.Rows[renglon].Cells["aMaternoProfesor"].Value=
							rd.GetString(rd.GetOrdinal("aMaterno"));
						tablaProfesores.Rows[renglon].Cells["correoProfesor"].Value=
							rd.GetString(rd.GetOrdinal("Correo"));
						tablaProfesores.Rows[renglon].Cells["telefonoProfesor"].Value=
							rd.GetString(rd.GetOrdinal("Telefono"));
						tablaProfesores.Rows[renglon].Cells["carreraProfesor"].Value=
							rd.GetString(rd.GetOrdinal("caNom"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		}
		
		//Actualizar profesor
		void BtnActualizarProfesorClick(object sender, EventArgs e)
		{
			String id = txtBuscarProfesor.Text;
			String codigo = txtCodigoProfesor.Text;
			String nombre= txtNombreProfesor.Text;
			String paterno = txtAPprofesor.Text;
			String materno = txtAMprofesor.Text;
			String telefono = txtTelProfesor.Text;
			String correo = txtMail.Text;
			String carrera = boxCarreraProfesor.SelectedValue.ToString();
			if (id == ""){
				
				MessageBox.Show("Igrese el codigo de la carrera");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					if(codigo != ""){
						query = "UPDATE profesor SET codigo="+ codigo+" WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}						
					if (nombre != ""){
						query = "UPDATE profesor SET nombre='"+ nombre+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (paterno != ""){
						query = "UPDATE profesor SET aPaterno='"+ paterno+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (materno != ""){
						query = "UPDATE profesor SET aMaterno='"+materno+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (telefono != ""){
						query = "UPDATE profesor SET telefono='"+telefono+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (correo != ""){
						query = "UPDATE profesor SET correo='"+correo+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					if (carrera != ""){
						query = "UPDATE profesor SET idCarrera='"+carrera+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
		}
		
		//Agregar Grupos
		void BtnAddGrupoClick(object sender, EventArgs e)
		{
			String codigo = txtCodigoGrupo.Text;
			String nombre=txtNombreGrupo.Text;
			String carrera = boxCarreraGrupo.SelectedValue.ToString();
			
			
			if(nombre == "" || codigo == "" || carrera ==""){
				MessageBox.Show("llene todos los campos");
			}
			else{
				MySqlConnection conexion= Conectar();
				
				try {
					String queryGrupo="INSERT INTO grupo (idGrupo,nombre) VALUES ("+codigo+",'"+nombre+"')";
					String queryCarreraGrupo = "INSERT INTO carrera_grupo (idGrupo, idCarrera) VALUES ("+codigo+","+carrera+")";
					String queryGradoGrupo;
					
					MySqlCommand comando = new MySqlCommand (queryGrupo, conexion);
					MySqlCommand comando2 = new MySqlCommand (queryCarreraGrupo, conexion);
					conexion.Open();
					
					comando.ExecuteNonQuery();
					comando2.ExecuteNonQuery();
					
					for(int i=0; i<6; i++){
						queryGradoGrupo = "INSERT INTO grupo_grado (idGrupo,idGrado) VALUES ("+codigo+","+(i+1)+")";
						MySqlCommand comando3 = new MySqlCommand (queryGradoGrupo, conexion);
						comando3.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha agregado el grupo");
					
					agregarGrupos();
					
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		}
		
		//boton para mostrar grupo
		void BtnMostrarGrupoClick(object sender, EventArgs e)
		{
			mostrarGrupos();
		}
		
		//mostrar grupos
		public void mostrarGrupos(){
			tablaGrupo.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT g.* , gra.nombre as grado, c.nombre as carrera FROM (grupo_grado gg ,carrera_grupo gc) " +
					"INNER JOIN grupo g ON gg.idGrupo=g.idGrupo and gc.idGrupo=g.idGrupo " +
					"INNER JOIN grado gra ON gg.idGrado=gra.idGrado " +
					"INNER JOIN carrera c ON gc.idCarrera=c.codigo;";
					
					
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
			
				while (rd.Read()) {
					int renglon = tablaGrupo.Rows.Add();
					tablaGrupo.Rows[renglon].Cells["idGrupo"].Value=
						rd.GetInt32(rd.GetOrdinal("idGrupo")).ToString();
					tablaGrupo.Rows[renglon].Cells["nombreGrupo"].Value=
						rd.GetString(rd.GetOrdinal("Nombre"));
					tablaGrupo.Rows[renglon].Cells["carreraGrupo"].Value=
						rd.GetString(rd.GetOrdinal("carrera")).ToString();
					tablaGrupo.Rows[renglon].Cells["gradoGrupo"].Value=
						rd.GetString(rd.GetOrdinal("grado"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Buscar grupo
		void BtnBuscarGrupoClick(object sender, EventArgs e)
		{
			String id = txtBuscarGrupo.Text;
			
			if (id ==""){
				MessageBox.Show("Ingrese el codigo del grupo a buscar");
			}else{
				tablaGrupo.Rows.Clear();
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "SELECT g.* , gra.nombre as grado, c.nombre as carrera FROM (grupo_grado gg ,carrera_grupo gc) " +
						"INNER JOIN grupo g ON gg.idGrupo=g.idGrupo and gc.idGrupo=g.idGrupo " +
						"INNER JOIN grado gra ON gg.idGrado=gra.idGrado " +
						"INNER JOIN carrera c ON gc.idCarrera=c.codigo WHERE gg.idGrupo="+id;
						
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaGrupo.Rows.Add();
						tablaGrupo.Rows[renglon].Cells["idGrupo"].Value=
							rd.GetInt32(rd.GetOrdinal("idGrupo")).ToString();
						tablaGrupo.Rows[renglon].Cells["nombreGrupo"].Value=
							rd.GetString(rd.GetOrdinal("Nombre"));
						tablaGrupo.Rows[renglon].Cells["carreraGrupo"].Value=
							rd.GetString(rd.GetOrdinal("carrera")).ToString();
						tablaGrupo.Rows[renglon].Cells["gradoGrupo"].Value=
							rd.GetString(rd.GetOrdinal("grado"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}	
			}
		}
		
		//Eliminar Grupo
		void BtnEliminarProductoClick(object sender, EventArgs e)
		{
			String codigo = txtBuscarGrupo.Text;
			MySqlConnection conexion = Conectar();
			
			if (codigo ==""){
				MessageBox.Show("Ingrese el codigo para eliminar");
			}else{
				try{
					DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				
					String query ="DELETE FROM grupo WHERE idGrupo="+codigo;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					if(dialogo == DialogResult.Yes){
						comando.ExecuteNonQuery();
						MessageBox.Show("Se ha eliminado");
					}else{
						MessageBox.Show("Se ha cancelado");
					}
					conexion.Close();
				}catch(MySqlException){
					MessageBox.Show("Error");
				}
			}
		}
		
		//Actualizar Grupo
		void BtnActualizarGrupoClick(object sender, EventArgs e)
		{
			//Por cambiar
			String codigo = txtCodigoGrupo.Text;
			String nombre= txtNombreGrupo.Text;
			String carrera = boxCarreraGrupo.SelectedValue.ToString();
			
			//Busqueda 
				String id = txtBuscarGrupo.Text;
			if (id == ""){
				
				MessageBox.Show("Ingrese el codigo del Grupo");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
											
					if (nombre != ""){
						query = "UPDATE grupo SET nombre='"+ nombre+"' WHERE idGrupo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (carrera != ""){
						query = "UPDATE carrera_grupo SET idCarrera='"+carrera+"' WHERE idGrupo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if(codigo != ""){
						query = "UPDATE grupo SET idGrupo="+ codigo+" WHERE idGrupo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
		}
		
		//Agregar Alumnos
		void BtnAgregarAlumnoClick(object sender, EventArgs e)
		{
			String nombre = txtNombreAlumno.Text;
			String codigo = txtCodigoAlumno.Text;
			String aPaterno = txtAPalumno.Text;
			String aMaterno = txtAMalumno.Text;
			String grupo = comboBoxAlumnoGrupo.SelectedValue.ToString();
			
			
			if (nombre == "" || codigo == "" || aPaterno == "" || aMaterno =="" || grupo ==""){
				MessageBox.Show("Llene todos los campos");
			}
			else{
				MySqlConnection conexion = Conectar();
				
			try {
					String query = "INSERT INTO estudiante (codigo, nombre, apellidoP, apellidoM, idGrupo_grado) " +
						"VALUES ("+codigo+",'"+nombre+"','"+aPaterno+"','"+aMaterno+"',"+grupo+")";
					
					MySqlCommand comando =new MySqlCommand(query, conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha agregado");
					
					agregarAlumnos();
					
				} catch (MySqlException) {
					MessageBox.Show("ERROR");
				}
			}
	
		}
		
		//Boton mostrar
		void BtnMostrarAlumnoClick(object sender, EventArgs e)
		{
			mostrarAlumno();
		}
		
		//Mostrar alumnos
		void mostrarAlumno(){
			dataGridAlumno.Visible = true;
			dataGridAlumno.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT est.codigo, est.nombre, est.apellidoP, est.apellidoM, gru.Nombre as grupo, gra.nombre as grado FROM grupo_grado gg " +
				" INNER JOIN estudiante est ON gg.id = est.idGrupo_grado "+
				" INNER JOIN grupo gru ON gg.idGrupo = gru.idGrupo "+
				" INNER JOIN grado gra ON gg.idGrado = gra.idGrado; ";
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
			
				while (rd.Read()) {
					int renglon = dataGridAlumno.Rows.Add();
					dataGridAlumno.Rows[renglon].Cells["codigoAlumno"].Value=
						rd.GetInt32(rd.GetOrdinal("codigo")).ToString();
					dataGridAlumno.Rows[renglon].Cells["nombreAlumno"].Value=
						rd.GetString(rd.GetOrdinal("nombre"));
					dataGridAlumno.Rows[renglon].Cells["aPaternoAlumno"].Value=
						rd.GetString(rd.GetOrdinal("apellidoP")).ToString();
					dataGridAlumno.Rows[renglon].Cells["aMaternoAlumno"].Value=
						rd.GetString(rd.GetOrdinal("apellidoM"));
					dataGridAlumno.Rows[renglon].Cells["grupoAlumno"].Value=
						rd.GetString(rd.GetOrdinal("grupo"));
					dataGridAlumno.Rows[renglon].Cells["gradoAlumno"].Value=
						rd.GetString(rd.GetOrdinal("grado"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Buscar alumno
		void Button2Click(object sender, EventArgs e)
		{	string codigo = txtBuscarAlumno.Text;
			dataGridAlumno.Visible = true;
			dataGridAlumno.Rows.Clear();
			MySqlConnection conexion = Conectar();
			if (codigo =="")
			{MessageBox.Show("Rellene campos");}
			else{
				try {
					String query = "SELECT est.codigo, est.nombre, est.apellidoP, est.apellidoM, gru.Nombre as grupo, gra.nombre as grado FROM grupo_grado gg " +
					" INNER JOIN estudiante est ON gg.id = est.idGrupo_grado "+
					" INNER JOIN grupo gru ON gg.idGrupo = gru.idGrupo "+
					" INNER JOIN grado gra ON gg.idGrado = gra.idGrado WHERE est.codigo ="+codigo;
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = dataGridAlumno.Rows.Add();
						dataGridAlumno.Rows[renglon].Cells["codigoAlumno"].Value=
							rd.GetInt32(rd.GetOrdinal("codigo")).ToString();
						dataGridAlumno.Rows[renglon].Cells["nombreAlumno"].Value=
							rd.GetString(rd.GetOrdinal("nombre"));
						dataGridAlumno.Rows[renglon].Cells["aPaternoAlumno"].Value=
							rd.GetString(rd.GetOrdinal("apellidoP")).ToString();
						dataGridAlumno.Rows[renglon].Cells["aMaternoAlumno"].Value=
							rd.GetString(rd.GetOrdinal("apellidoM"));
						dataGridAlumno.Rows[renglon].Cells["grupoAlumno"].Value=
							rd.GetString(rd.GetOrdinal("grupo"));
						dataGridAlumno.Rows[renglon].Cells["gradoAlumno"].Value=
							rd.GetString(rd.GetOrdinal("grado"));
					}
					
						conexion.Close();
				}catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		}
		
		//Eliminar alumno
		void BtnEliminarAlumnoClick(object sender, EventArgs e)
		{
			String codigo = txtBuscarAlumno.Text;
			MySqlConnection conexion = Conectar();
			
			if (codigo ==""){
				MessageBox.Show("Ingrese el codigo para eliminar");
			}else{
				try{
					DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				
					String query ="DELETE FROM estudiante WHERE codigo ="+codigo;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					if(dialogo == DialogResult.Yes){
						comando.ExecuteNonQuery();
						MessageBox.Show("Se ha eliminado");
					}else{
						MessageBox.Show("Se ha cancelado");
					}
					conexion.Close();
				}catch(MySqlException){
					MessageBox.Show("Error");
				}
			}
		}
		
		//Actualizar alumno
		void BtnActualizarAlumnoClick(object sender, EventArgs e)
		{
			
			String codigo = txtCodigoAlumno.Text;
			String nombre= txtNombreAlumno.Text;
			String aPaterno = txtAPalumno.Text;
			String aMaterno = txtAMalumno.Text;
			String grupo = comboBoxAlumnoGrupo.SelectedValue.ToString();
			
			//Busqueda 
				String id = txtBuscarAlumno.Text;
			if (id == ""){
				
				MessageBox.Show("Ingrese el codigo del Alumno");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					
					if (grupo != ""){
						query = "UPDATE estudiante SET idGrupo_grado="+ grupo+" WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();	}
					if (nombre != ""){
						query = "UPDATE estudiante SET nombre='"+ nombre+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if (aPaterno != ""){
						query = "UPDATE estudiante SET apellidoP='"+aPaterno+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if(aMaterno !=""){
						query = "UPDATE estudiante SET apellidoM= '"+ aMaterno+"' WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if(codigo != ""){
						query = "UPDATE estudiante SET codigo="+ codigo+" WHERE codigo="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
		}
		
		//Agregar Asignatura
		void BtnAddAsignaturaClick(object sender, EventArgs e)
		{
			String codigo = txtCodigoAsignatura.Text;
			String nombre= txtNombreAsignatura.Text;
			String creditos = txtCreditosAsignatura.Text;
			
			if(nombre == "" || codigo == "" || creditos ==""){
				MessageBox.Show("llene todos los campos");
			}
			else{
				MySqlConnection conexion= Conectar();
				
				try {
					String query="INSERT INTO asignatura (idAsignatura,nombre,creditos) VALUES ("+codigo+",'"+nombre+"',"+creditos+")";
					
					MySqlCommand comando = new MySqlCommand (query, conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha agregado la asignatura");
					agregarAsignatura();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}			

		}
		
		//Vinculacion con grado y carrera
		void BtnVincularAsignaturaClick(object sender, EventArgs e)
		{
			String asignatura = txtCodigoAsignatura.Text;
			String carrera = boxCarreraAsignatura.SelectedValue.ToString();
			String grado = boxGradoAsignatura.SelectedValue.ToString();
		
			if(asignatura == "" || carrera =="" || grado == ""){
				MessageBox.Show("llene todos los campos");
			}
			else{
				MySqlConnection conexion= Conectar();
				
				try {
				
					String queryC="INSERT INTO carrera_asignatura (idCarrera,idAsignatura, idGrado) VALUES ("+carrera+","+asignatura+","+grado+")";
					//String queryG="INSERT INTO asignatura_grado (idAsignatura,idGrado) VALUES ("+asignatura+","+grado+")";
					MySqlCommand comandoC = new MySqlCommand(queryC,conexion);
					//MySqlCommand comandoG = new MySqlCommand(queryG,conexion);
				
					conexion.Open();
					
					comandoC.ExecuteNonQuery();
					//comandoG.ExecuteNonQuery();
					
					conexion.Close();
					MessageBox.Show("Se ha vinculado");
					
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		}
		
		//Boton Mostar asignatura
		void BtnMostarAsignaturaClick(object sender, EventArgs e)
		{
			mostrarAsignatura();
		}
		
		//Mostrar asignatura
		public void mostrarAsignatura(){
			tablaAsignatura.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT a.* , gra.nombre as grado, c.nombre as carrera FROM carrera_asignatura ca " +
					"INNER JOIN asignatura a ON ca.idAsignatura = a.idAsignatura " +
					"INNER JOIN grado gra ON ca.idGrado=gra.idGrado " +
					"INNER JOIN carrera c ON ca.idCarrera=c.codigo;";
					
					
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
			
				while (rd.Read()) {
					int renglon = tablaAsignatura.Rows.Add();
					
					tablaAsignatura.Rows[renglon].Cells["codigoAsignatura"].Value=
						rd.GetInt32(rd.GetOrdinal("idAsignatura")).ToString();
					tablaAsignatura.Rows[renglon].Cells["nombreAsignatura"].Value=
						rd.GetString(rd.GetOrdinal("Nombre"));
					tablaAsignatura.Rows[renglon].Cells["creditosAsignatura"].Value=
						rd.GetString(rd.GetOrdinal("creditos")).ToString();
					tablaAsignatura.Rows[renglon].Cells["gradoAsignatura"].Value=
						rd.GetString(rd.GetOrdinal("grado"));
					tablaAsignatura.Rows[renglon].Cells["carreraAsignatrura"].Value=
						rd.GetString(rd.GetOrdinal("carrera"));
					
					
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Buscar asignatura
		void BtnBuscarAsignaturaClick(object sender, EventArgs e)
		{
			String id = txtBuscarAsignatura.Text;
			tablaAsignatura.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			if(id == ""){
				MessageBox.Show("Especifique la asignatura");
			}
			else{
				
			
				try {
					
					String query = "SELECT a.* , gra.nombre as grado, c.nombre as carrera FROM carrera_asignatura ca " +
						"INNER JOIN asignatura a ON ca.idAsignatura = a.idAsignatura " +
						"INNER JOIN grado gra ON ca.idGrado=gra.idGrado " +
						"INNER JOIN carrera c ON ca.idCarrera=c.codigo WHERE a.idAsignatura="+id;
						
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaAsignatura.Rows.Add();
						tablaAsignatura.Rows[renglon].Cells["codigoAsignatura"].Value=
							rd.GetInt32(rd.GetOrdinal("idAsignatura")).ToString();
						tablaAsignatura.Rows[renglon].Cells["nombreAsignatura"].Value=
							rd.GetString(rd.GetOrdinal("Nombre"));
						tablaAsignatura.Rows[renglon].Cells["creditosAsignatura"].Value=
							rd.GetString(rd.GetOrdinal("creditos")).ToString();
						tablaAsignatura.Rows[renglon].Cells["gradoAsignatura"].Value=
							rd.GetString(rd.GetOrdinal("grado"));
						tablaAsignatura.Rows[renglon].Cells["carreraAsignatrura"].Value=
							rd.GetString(rd.GetOrdinal("carrera"));
						
						
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		
		}
		
		//Eliminar asignatura
		void BtnEliminarClick(object sender, EventArgs e)
		{
			String codigo = txtBuscarAsignatura.Text;
			MySqlConnection conexion = Conectar();
			
			if (codigo ==""){
				MessageBox.Show("Ingrese el codigo para eliminar");
			}else{
				try{
					DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				
					String query ="DELETE FROM asignatura WHERE idAsignatura ="+codigo;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					if(dialogo == DialogResult.Yes){
						comando.ExecuteNonQuery();
						MessageBox.Show("Se ha eliminado");
					}else{
						MessageBox.Show("Se ha cancelado");
					}
					conexion.Close();
				}catch(MySqlException){
					MessageBox.Show("Error");
				}
			}
		}
		
		//Actualizar asignaturas
		void BtnActualizarAsignaturaClick(object sender, EventArgs e)
		{
			String codigo = txtCodigoAsignatura.Text;
			String nombre= txtNombreAsignatura.Text;
			String creditos = txtCreditosAsignatura.Text;
			
			String id = txtBuscarAsignatura.Text;
			
			if (id == ""){
				
				MessageBox.Show("Ingrese el codigo del Alumno");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					
					if (nombre != ""){
						query = "UPDATE asignatura SET nombre='"+nombre+"' WHERE idAsignatura="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();	}
					if (creditos != ""){
						query = "UPDATE asignatura SET creditos="+ creditos+" WHERE idAsignatura="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if(codigo != ""){
						query = "UPDATE asignatura SET idAsignatura="+codigo+" WHERE idAsignatura="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}
			
		}
		
		//Agrega clases (profesor_asignatura)
		void BtnAddClasesClick(object sender, EventArgs e)
		{
			String profesor = boxProfesorClases.SelectedValue.ToString();
			String grupo = boxGrupoClases.SelectedValue.ToString();
			String asignatura = boxAsignaturaClases.SelectedValue.ToString();
			
			if(profesor == "" || grupo == "" || asignatura == ""){
				MessageBox.Show("Rellene todos los campos");
			}else{
				MySqlConnection conexion = Conectar();
				try {
					String query ="INSERT INTO profesor_asignatura (idProfesor,idGrupo_grado,idAsignatura) VALUES("+profesor+","+grupo+","+asignatura+")";
					MySqlCommand comando = new MySqlCommand(query, conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					
					MessageBox.Show("Se le ha asigando la clase a este profesor");
				} catch (MySqlException) {
					MessageBox.Show("ERROR");
				}
			}
		}
		
		//Boton Mostrar clases
		void BtnMostrarClasesClick(object sender, EventArgs e)
		{
			mostrarClases();
		}
		
		//Mostrar clases
		public void mostrarClases(){
			tablaClase.Rows.Clear();
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT pa.id, CONCAT(p.aPaterno,' ',p.aMaterno,' ',p.nombre) as profe, a.nombre as materia, CONCAT(gru.Nombre,' ', gra.Nombre) as gru " +
					"FROM (profesor_asignatura pa, grupo_grado g) " +
					"INNER JOIN profesor p ON p.Codigo = pa.idProfesor " +
					"INNER JOIN asignatura a ON a.idAsignatura = pa.idAsignatura " +
					"INNER JOIN grupo gru ON g.idGrupo = gru.idGrupo AND pa.idGrupo_grado = g.id " +
					"INNER JOIN grado gra ON gra.idGrado = g.idGrado";
					
				MySqlCommand comando =new MySqlCommand (query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
			
				while (rd.Read()) {
					int renglon = tablaClase.Rows.Add();
					
					tablaClase.Rows[renglon].Cells["codigoClase"].Value=
						rd.GetInt32(rd.GetOrdinal("id")).ToString();
					tablaClase.Rows[renglon].Cells["profesorClase"].Value=
						rd.GetString(rd.GetOrdinal("profe"));
					tablaClase.Rows[renglon].Cells["materiaClase"].Value=
						rd.GetString(rd.GetOrdinal("materia"));
					tablaClase.Rows[renglon].Cells["grupoClase"].Value=
						rd.GetString(rd.GetOrdinal("gru"));
				}
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("error");
			}
		}
		
		//Eliminar Clase
		void BtnEliminarClaseClick(object sender, EventArgs e)
		{
			String codigo = txtBuscarClase.Text;
			MySqlConnection conexion = Conectar();
			
			if (codigo ==""){
				MessageBox.Show("Ingrese el codigo para eliminar");
			}else{
				try{
					DialogResult dialogo= MessageBox.Show ("Estas seguro que quieres eliminar?","!ADVERTENCIA!",MessageBoxButtons.YesNoCancel);
				
					String query ="DELETE FROM profesor_asignatura WHERE id ="+codigo;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					if(dialogo == DialogResult.Yes){
						comando.ExecuteNonQuery();
						MessageBox.Show("Se ha eliminado");
					}else{
						MessageBox.Show("Se ha cancelado");
					}
					conexion.Close();
				}catch(MySqlException){
					MessageBox.Show("Error");
				}
			}	
		}
		
		//Buscar Clases
		void BtnBuscarClaseClick(object sender, EventArgs e)
		{
			tablaClase.Rows.Clear();
			
			String id = txtBuscarClase.Text;
			
			if (id ==""){
				MessageBox.Show("Ingrese el codigo de la clase");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "SELECT pa.id, CONCAT(p.aPaterno,' ',p.aMaterno,' ',p.nombre) as profe, a.nombre as materia, CONCAT(gru.Nombre,' ', gra.Nombre) as gru " +
						"FROM (profesor_asignatura pa, grupo_grado g) " +
						"INNER JOIN profesor p ON p.Codigo = pa.idProfesor " +
						"INNER JOIN asignatura a ON a.idAsignatura = pa.idAsignatura " +
						"INNER JOIN grupo gru ON g.idGrupo = gru.idGrupo AND pa.idGrupo_grado = g.id " +
						"INNER JOIN grado gra ON gra.idGrado = g.idGrado WHERE pa.id="+id;
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaClase.Rows.Add();
						
						tablaClase.Rows[renglon].Cells["codigoClase"].Value=
							rd.GetInt32(rd.GetOrdinal("id")).ToString();
						tablaClase.Rows[renglon].Cells["profesorClase"].Value=
							rd.GetString(rd.GetOrdinal("profe"));
						tablaClase.Rows[renglon].Cells["materiaClase"].Value=
							rd.GetString(rd.GetOrdinal("materia"));
						tablaClase.Rows[renglon].Cells["grupoClase"].Value=
							rd.GetString(rd.GetOrdinal("gru"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			}
		}
		
		//Actualizar Clases
		void BtnActualizarClick(object sender, EventArgs e)
		{
			String profe = boxProfesorClases.SelectedValue.ToString();
			String materia= boxAsignaturaClases.SelectedValue.ToString();
			String grupo = boxGrupoClases.SelectedValue.ToString();
			
			String id = txtBuscarClase.Text;
			
			if (id == ""){
				
				MessageBox.Show("Ingrese el codigo del Alumno");
			}else{
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query;
					
					MySqlCommand comando;;
					conexion.Open();
					
					if (profe != ""){
						query = "UPDATE profesor_asignatura SET idProfesor='"+profe+"' WHERE id="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();	}
					if (materia != ""){
						query = "UPDATE profesor_asignatura SET idAsignatura="+materia+" WHERE id="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					if(grupo != ""){
						query = "UPDATE profesor_asignatura SET idGrupo_grado="+grupo+" WHERE id="+id;
						comando = new MySqlCommand(query, conexion);
						comando.ExecuteNonQuery();
					}
					
					conexion.Close();
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("¡ ¡ ¡ ERROR ! ! !");
				}
			}

		}
		
		// Agregar Usuario Profesor
		void BtnAddUsuarioPClick(object sender, EventArgs e)
		{
			String codigo = boxMaestroUsuario.SelectedValue.ToString();
			String contraseña = txtContrasena.Text;
			String confirmar = txtConfContrasena.Text;
			
			if(codigo == ""){
				MessageBox.Show("Ingrese el usuario correspondiente");
			}else{
				if(contraseña.Equals(confirmar)){
					
					MySqlConnection conexion = Conectar();
					try {
						String query = "INSERT INTO usuario VALUES ("+codigo+",'"+contraseña+"',1)";
						MySqlCommand comando = new MySqlCommand(query, conexion);
						
						conexion.Open();
						 
						comando.ExecuteNonQuery();
						
						conexion.Close();
					} catch (MySqlException) {
						
						MessageBox.Show("Error");
					}
				}else{
					MessageBox.Show("contraseña diferente a la de la confirmacion");
				}
			}
		}
		void BtnAddUsuarioEClick(object sender, EventArgs e)
		{
			String codigo = boxEstudianteUsuario.SelectedValue.ToString();
			String contraseña = txtContrasena.Text;
			String confirmar = txtConfContrasena.Text;
			
			if(codigo == ""){
				MessageBox.Show("Ingrese el usuario correspondiente");
			}else{
				if(contraseña.Equals(confirmar)){
					
					MySqlConnection conexion = Conectar();
					try {
						String query = "INSERT INTO usuario VALUES ("+codigo+",'"+contraseña+"',2)";
						MySqlCommand comando = new MySqlCommand(query, conexion);
						
						conexion.Open();
						 
						comando.ExecuteNonQuery();
						
						conexion.Close();
					} catch (MySqlException) {
						
						MessageBox.Show("Error");
					}
				}else{
					MessageBox.Show("contraseña diferente a la de la confirmacion");
				}
			}
		}
		
	}
}
