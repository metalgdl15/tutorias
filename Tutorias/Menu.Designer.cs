﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 25/11/2018
 * Time: 18:25
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Tutorias
{
	partial class Menu
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TabControl tabMenu;
		private System.Windows.Forms.TabPage tabCarrera;
		private System.Windows.Forms.Label lblNivelAddCarrera;
		private System.Windows.Forms.Label lblAreaAddCarrera;
		private System.Windows.Forms.Label lblNombreAddCarrera;
		private System.Windows.Forms.Button btnAddCarrera;
		private System.Windows.Forms.TextBox txtAreaCarrera;
		private System.Windows.Forms.TextBox txtNombreCarrera;
		private System.Windows.Forms.Button btnViewCarrera;
		private System.Windows.Forms.DataGridView tablaCarreras;
		private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
		private System.Windows.Forms.DataGridViewTextBoxColumn Nivel;
		private System.Windows.Forms.DataGridViewTextBoxColumn Area;
		private System.Windows.Forms.TextBox txtSearchCarrera;
		private System.Windows.Forms.Button btnSearchCarrera;
		private System.Windows.Forms.Label lblSearchCarrera;
		private System.Windows.Forms.Button btnUpdateCarrera;
		private System.Windows.Forms.Button btnEliminarCarrera;
		private System.Windows.Forms.TabPage tabAlumno;
		private System.Windows.Forms.TabPage tabAsignatura;
		private System.Windows.Forms.TabPage tabGrupo;
		private System.Windows.Forms.TabPage tabProfesor;
		private System.Windows.Forms.TabPage tabPeriodo;
		private System.Windows.Forms.TextBox txtNivelCarrera;
		private System.Windows.Forms.DateTimePicker dateInicioPeriodo;
		private System.Windows.Forms.Label lblInicioPeriodo;
		private System.Windows.Forms.Label lblNombrePeriodo;
		private System.Windows.Forms.Button btnMostrarPeriodo;
		private System.Windows.Forms.Button btnAgregarPeriodo;
		private System.Windows.Forms.TextBox txtNombrePeriodo;
		private System.Windows.Forms.DateTimePicker dateFinPeriodo;
		private System.Windows.Forms.Label lblFinPeriodo;
		private System.Windows.Forms.DataGridView tablaPeriodos;
		private System.Windows.Forms.DataGridViewTextBoxColumn idPeriodo;
		private System.Windows.Forms.DataGridViewTextBoxColumn fechaInicio;
		private System.Windows.Forms.DataGridViewTextBoxColumn fechaFin;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombrePeriodo;
		private System.Windows.Forms.Button btnActualizarPeriodo;
		private System.Windows.Forms.Button btnEliminarPeriodo;
		private System.Windows.Forms.TextBox txtIdPeriodo;
		private System.Windows.Forms.Label lblIdPeriodo;
		private System.Windows.Forms.Button btnAgregarProfesor;
		private System.Windows.Forms.ComboBox boxCarreraProfesor;
		private System.Windows.Forms.TextBox txtTelProfesor;
		private System.Windows.Forms.TextBox txtMail;
		private System.Windows.Forms.TextBox txtAMprofesor;
		private System.Windows.Forms.TextBox txtAPprofesor;
		private System.Windows.Forms.TextBox txtNombreProfesor;
		private System.Windows.Forms.TextBox txtCodigoProfesor;
		private System.Windows.Forms.Label lblCarreraProfesor;
		private System.Windows.Forms.Label lblTelefonoProfesor;
		private System.Windows.Forms.Label lblCorreoProfesor;
		private System.Windows.Forms.Label lblApellidoM;
		private System.Windows.Forms.Label lblApellidoP;
		private System.Windows.Forms.Label lblNombreProfesor;
		private System.Windows.Forms.Label lblCodigoProfesor;
		private System.Windows.Forms.DataGridView tablaProfesores;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn aPaternoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn aMaternoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn correoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn telefonoProfesor;
		private System.Windows.Forms.DataGridViewTextBoxColumn carreraProfesor;
		private System.Windows.Forms.Button btnMostrarProfesores;
		private System.Windows.Forms.Button btnBuscarProfesor;
		private System.Windows.Forms.Button btnEliminarProfesor;
		private System.Windows.Forms.Label lblBuscarProfesor;
		private System.Windows.Forms.TextBox txtBuscarProfesor;
		private System.Windows.Forms.Button btnActualizarProfesor;
		private System.Windows.Forms.TextBox txtNombreGrupo;
		private System.Windows.Forms.TextBox txtCodigoGrupo;
		private System.Windows.Forms.ComboBox boxCarreraGrupo;
		private System.Windows.Forms.Label lblCarreraGrupo;
		private System.Windows.Forms.Label lblNombreGrupo;
		private System.Windows.Forms.Label lblCodigoGrupo;
		private System.Windows.Forms.Button btnAddGrupo;
		private System.Windows.Forms.Button btnMostrarGrupo;
		private System.Windows.Forms.DataGridView tablaGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn idGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn carreraGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn gradoGrupo;
		private System.Windows.Forms.Button btnActualizarGrupo;
		private System.Windows.Forms.Button btnEliminarProducto;
		private System.Windows.Forms.Button btnBuscarGrupo;
		private System.Windows.Forms.TextBox txtBuscarGrupo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtAMalumno;
		private System.Windows.Forms.TextBox txtAPalumno;
		private System.Windows.Forms.TextBox txtNombreAlumno;
		private System.Windows.Forms.TextBox txtCodigoAlumno;
		private System.Windows.Forms.Label labelAMalumno;
		private System.Windows.Forms.Label lblAPalumno;
		private System.Windows.Forms.Label lblNombreAlumno;
		private System.Windows.Forms.Label lblAlumnoCodigo;
		private System.Windows.Forms.DataGridView dataGridAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn aPaternoAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn aMaternoAlumno;
		private System.Windows.Forms.Button btnMostrarAlumno;
		private System.Windows.Forms.Button btnAgregarAlumno;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxAlumnoGrupo;
		private System.Windows.Forms.DataGridViewTextBoxColumn grupoAlumno;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtBuscarAlumno;
		private System.Windows.Forms.Button btnActualizarAlumno;
		private System.Windows.Forms.Button btnAlumnoBuscar;
		private System.Windows.Forms.Button btnEliminarAlumno;
		private System.Windows.Forms.DataGridViewTextBoxColumn gradoAlumno;
		private System.Windows.Forms.Label lblCodigoAsignatura;
		private System.Windows.Forms.TextBox txtCreditosAsignatura;
		private System.Windows.Forms.TextBox txtNombreAsignatura;
		private System.Windows.Forms.Label lblVinculo;
		private System.Windows.Forms.Label lblCreditosAsignatura;
		private System.Windows.Forms.Label lblNombreAsignatura;
		private System.Windows.Forms.ComboBox boxCarreraAsignatura;
		private System.Windows.Forms.Button btnBuscarAsignatura;
		private System.Windows.Forms.Button btnMostarAsignatura;
		private System.Windows.Forms.DataGridView tablaAsignatura;
		private System.Windows.Forms.Button btnVincularAsignatura;
		private System.Windows.Forms.Button btnAddAsignatura;
		private System.Windows.Forms.TextBox txtCodigoAsignatura;
		private System.Windows.Forms.ComboBox boxGradoAsignatura;
		private System.Windows.Forms.Button btnActualizarAsignatura;
		private System.Windows.Forms.Button btnEliminar;
		private System.Windows.Forms.Label lblBusquedaAsignatura;
		private System.Windows.Forms.TextBox txtBuscarAsignatura;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoAsignatura;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreAsignatura;
		private System.Windows.Forms.DataGridViewTextBoxColumn creditosAsignatura;
		private System.Windows.Forms.DataGridViewTextBoxColumn carreraAsignatrura;
		private System.Windows.Forms.DataGridViewTextBoxColumn gradoAsignatura;
		private System.Windows.Forms.TabPage tabClases;
		private System.Windows.Forms.Label lblAsignaturaClases;
		private System.Windows.Forms.Label lblProfesorClases;
		private System.Windows.Forms.Label lblGrupoClases;
		private System.Windows.Forms.ComboBox boxGrupoClases;
		private System.Windows.Forms.Button btnAddClases;
		private System.Windows.Forms.ComboBox boxAsignaturaClases;
		private System.Windows.Forms.ComboBox boxProfesorClases;
		private System.Windows.Forms.DataGridView tablaClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigoClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn profesorClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn materiaClase;
		private System.Windows.Forms.DataGridViewTextBoxColumn grupoClase;
		private System.Windows.Forms.Button btnMostrarClases;
		private System.Windows.Forms.Button btnActualizar;
		private System.Windows.Forms.TextBox txtBuscarClase;
		private System.Windows.Forms.Label lblEliminarClase;
		private System.Windows.Forms.Button btnEliminarClase;
		private System.Windows.Forms.Button btnBuscarClase;
		private System.Windows.Forms.TabPage tabUsuarios;
		private System.Windows.Forms.TextBox txtContrasena;
		private System.Windows.Forms.Label lblMaestro;
		private System.Windows.Forms.Button btnAddUsuarioE;
		private System.Windows.Forms.Button btnAddUsuarioP;
		private System.Windows.Forms.ComboBox boxEstudianteUsuario;
		private System.Windows.Forms.Label lblConfirmarContraseña;
		private System.Windows.Forms.TextBox txtConfContrasena;
		private System.Windows.Forms.ComboBox boxMaestroUsuario;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label lblCotraseña;
		private System.Windows.Forms.Label label2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
			this.tabPeriodo = new System.Windows.Forms.TabPage();
			this.btnActualizarPeriodo = new System.Windows.Forms.Button();
			this.btnEliminarPeriodo = new System.Windows.Forms.Button();
			this.txtIdPeriodo = new System.Windows.Forms.TextBox();
			this.txtNombrePeriodo = new System.Windows.Forms.TextBox();
			this.lblIdPeriodo = new System.Windows.Forms.Label();
			this.tablaPeriodos = new System.Windows.Forms.DataGridView();
			this.idPeriodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fechaInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fechaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombrePeriodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnMostrarPeriodo = new System.Windows.Forms.Button();
			this.btnAgregarPeriodo = new System.Windows.Forms.Button();
			this.dateFinPeriodo = new System.Windows.Forms.DateTimePicker();
			this.lblFinPeriodo = new System.Windows.Forms.Label();
			this.dateInicioPeriodo = new System.Windows.Forms.DateTimePicker();
			this.lblInicioPeriodo = new System.Windows.Forms.Label();
			this.lblNombrePeriodo = new System.Windows.Forms.Label();
			this.tabProfesor = new System.Windows.Forms.TabPage();
			this.btnActualizarProfesor = new System.Windows.Forms.Button();
			this.lblBuscarProfesor = new System.Windows.Forms.Label();
			this.txtBuscarProfesor = new System.Windows.Forms.TextBox();
			this.btnBuscarProfesor = new System.Windows.Forms.Button();
			this.btnEliminarProfesor = new System.Windows.Forms.Button();
			this.btnMostrarProfesores = new System.Windows.Forms.Button();
			this.tablaProfesores = new System.Windows.Forms.DataGridView();
			this.codigoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.aPaternoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.aMaternoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.correoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.telefonoProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.carreraProfesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnAgregarProfesor = new System.Windows.Forms.Button();
			this.boxCarreraProfesor = new System.Windows.Forms.ComboBox();
			this.txtTelProfesor = new System.Windows.Forms.TextBox();
			this.txtMail = new System.Windows.Forms.TextBox();
			this.txtAMprofesor = new System.Windows.Forms.TextBox();
			this.txtAPprofesor = new System.Windows.Forms.TextBox();
			this.txtNombreProfesor = new System.Windows.Forms.TextBox();
			this.txtCodigoProfesor = new System.Windows.Forms.TextBox();
			this.lblCarreraProfesor = new System.Windows.Forms.Label();
			this.lblTelefonoProfesor = new System.Windows.Forms.Label();
			this.lblCorreoProfesor = new System.Windows.Forms.Label();
			this.lblApellidoM = new System.Windows.Forms.Label();
			this.lblApellidoP = new System.Windows.Forms.Label();
			this.lblNombreProfesor = new System.Windows.Forms.Label();
			this.lblCodigoProfesor = new System.Windows.Forms.Label();
			this.tabAsignatura = new System.Windows.Forms.TabPage();
			this.btnActualizarAsignatura = new System.Windows.Forms.Button();
			this.btnEliminar = new System.Windows.Forms.Button();
			this.lblBusquedaAsignatura = new System.Windows.Forms.Label();
			this.txtBuscarAsignatura = new System.Windows.Forms.TextBox();
			this.btnBuscarAsignatura = new System.Windows.Forms.Button();
			this.btnMostarAsignatura = new System.Windows.Forms.Button();
			this.tablaAsignatura = new System.Windows.Forms.DataGridView();
			this.codigoAsignatura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreAsignatura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.creditosAsignatura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.carreraAsignatrura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.gradoAsignatura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnVincularAsignatura = new System.Windows.Forms.Button();
			this.btnAddAsignatura = new System.Windows.Forms.Button();
			this.txtCodigoAsignatura = new System.Windows.Forms.TextBox();
			this.boxGradoAsignatura = new System.Windows.Forms.ComboBox();
			this.boxCarreraAsignatura = new System.Windows.Forms.ComboBox();
			this.lblCodigoAsignatura = new System.Windows.Forms.Label();
			this.txtCreditosAsignatura = new System.Windows.Forms.TextBox();
			this.txtNombreAsignatura = new System.Windows.Forms.TextBox();
			this.lblVinculo = new System.Windows.Forms.Label();
			this.lblCreditosAsignatura = new System.Windows.Forms.Label();
			this.lblNombreAsignatura = new System.Windows.Forms.Label();
			this.tabAlumno = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.txtBuscarAlumno = new System.Windows.Forms.TextBox();
			this.btnActualizarAlumno = new System.Windows.Forms.Button();
			this.btnAlumnoBuscar = new System.Windows.Forms.Button();
			this.btnEliminarAlumno = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxAlumnoGrupo = new System.Windows.Forms.ComboBox();
			this.dataGridAlumno = new System.Windows.Forms.DataGridView();
			this.codigoAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.aPaternoAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.aMaternoAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grupoAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.gradoAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnMostrarAlumno = new System.Windows.Forms.Button();
			this.btnAgregarAlumno = new System.Windows.Forms.Button();
			this.txtAMalumno = new System.Windows.Forms.TextBox();
			this.txtAPalumno = new System.Windows.Forms.TextBox();
			this.txtNombreAlumno = new System.Windows.Forms.TextBox();
			this.txtCodigoAlumno = new System.Windows.Forms.TextBox();
			this.labelAMalumno = new System.Windows.Forms.Label();
			this.lblAPalumno = new System.Windows.Forms.Label();
			this.lblNombreAlumno = new System.Windows.Forms.Label();
			this.lblAlumnoCodigo = new System.Windows.Forms.Label();
			this.tabCarrera = new System.Windows.Forms.TabPage();
			this.btnUpdateCarrera = new System.Windows.Forms.Button();
			this.btnEliminarCarrera = new System.Windows.Forms.Button();
			this.lblNombreAddCarrera = new System.Windows.Forms.Label();
			this.btnSearchCarrera = new System.Windows.Forms.Button();
			this.txtSearchCarrera = new System.Windows.Forms.TextBox();
			this.txtNombreCarrera = new System.Windows.Forms.TextBox();
			this.txtAreaCarrera = new System.Windows.Forms.TextBox();
			this.txtNivelCarrera = new System.Windows.Forms.TextBox();
			this.lblAreaAddCarrera = new System.Windows.Forms.Label();
			this.lblSearchCarrera = new System.Windows.Forms.Label();
			this.tablaCarreras = new System.Windows.Forms.DataGridView();
			this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Nivel = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Area = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnViewCarrera = new System.Windows.Forms.Button();
			this.lblNivelAddCarrera = new System.Windows.Forms.Label();
			this.btnAddCarrera = new System.Windows.Forms.Button();
			this.tabMenu = new System.Windows.Forms.TabControl();
			this.tabGrupo = new System.Windows.Forms.TabPage();
			this.btnActualizarGrupo = new System.Windows.Forms.Button();
			this.btnEliminarProducto = new System.Windows.Forms.Button();
			this.btnBuscarGrupo = new System.Windows.Forms.Button();
			this.txtBuscarGrupo = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tablaGrupo = new System.Windows.Forms.DataGridView();
			this.idGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.carreraGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.gradoGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnMostrarGrupo = new System.Windows.Forms.Button();
			this.btnAddGrupo = new System.Windows.Forms.Button();
			this.txtNombreGrupo = new System.Windows.Forms.TextBox();
			this.txtCodigoGrupo = new System.Windows.Forms.TextBox();
			this.boxCarreraGrupo = new System.Windows.Forms.ComboBox();
			this.lblCarreraGrupo = new System.Windows.Forms.Label();
			this.lblNombreGrupo = new System.Windows.Forms.Label();
			this.lblCodigoGrupo = new System.Windows.Forms.Label();
			this.tabClases = new System.Windows.Forms.TabPage();
			this.btnBuscarClase = new System.Windows.Forms.Button();
			this.btnActualizar = new System.Windows.Forms.Button();
			this.txtBuscarClase = new System.Windows.Forms.TextBox();
			this.lblEliminarClase = new System.Windows.Forms.Label();
			this.btnEliminarClase = new System.Windows.Forms.Button();
			this.tablaClase = new System.Windows.Forms.DataGridView();
			this.codigoClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.profesorClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.materiaClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grupoClase = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnMostrarClases = new System.Windows.Forms.Button();
			this.btnAddClases = new System.Windows.Forms.Button();
			this.boxAsignaturaClases = new System.Windows.Forms.ComboBox();
			this.boxProfesorClases = new System.Windows.Forms.ComboBox();
			this.lblAsignaturaClases = new System.Windows.Forms.Label();
			this.lblProfesorClases = new System.Windows.Forms.Label();
			this.lblGrupoClases = new System.Windows.Forms.Label();
			this.boxGrupoClases = new System.Windows.Forms.ComboBox();
			this.tabUsuarios = new System.Windows.Forms.TabPage();
			this.btnAddUsuarioE = new System.Windows.Forms.Button();
			this.btnAddUsuarioP = new System.Windows.Forms.Button();
			this.boxEstudianteUsuario = new System.Windows.Forms.ComboBox();
			this.lblConfirmarContraseña = new System.Windows.Forms.Label();
			this.txtConfContrasena = new System.Windows.Forms.TextBox();
			this.boxMaestroUsuario = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.lblCotraseña = new System.Windows.Forms.Label();
			this.txtContrasena = new System.Windows.Forms.TextBox();
			this.lblMaestro = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tabPeriodo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaPeriodos)).BeginInit();
			this.tabProfesor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaProfesores)).BeginInit();
			this.tabAsignatura.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaAsignatura)).BeginInit();
			this.tabAlumno.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAlumno)).BeginInit();
			this.tabCarrera.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaCarreras)).BeginInit();
			this.tabMenu.SuspendLayout();
			this.tabGrupo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaGrupo)).BeginInit();
			this.tabClases.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaClase)).BeginInit();
			this.tabUsuarios.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabPeriodo
			// 
			this.tabPeriodo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPeriodo.BackgroundImage")));
			this.tabPeriodo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabPeriodo.Controls.Add(this.btnActualizarPeriodo);
			this.tabPeriodo.Controls.Add(this.btnEliminarPeriodo);
			this.tabPeriodo.Controls.Add(this.txtIdPeriodo);
			this.tabPeriodo.Controls.Add(this.txtNombrePeriodo);
			this.tabPeriodo.Controls.Add(this.lblIdPeriodo);
			this.tabPeriodo.Controls.Add(this.tablaPeriodos);
			this.tabPeriodo.Controls.Add(this.btnMostrarPeriodo);
			this.tabPeriodo.Controls.Add(this.btnAgregarPeriodo);
			this.tabPeriodo.Controls.Add(this.dateFinPeriodo);
			this.tabPeriodo.Controls.Add(this.lblFinPeriodo);
			this.tabPeriodo.Controls.Add(this.dateInicioPeriodo);
			this.tabPeriodo.Controls.Add(this.lblInicioPeriodo);
			this.tabPeriodo.Controls.Add(this.lblNombrePeriodo);
			this.tabPeriodo.Location = new System.Drawing.Point(4, 22);
			this.tabPeriodo.Name = "tabPeriodo";
			this.tabPeriodo.Padding = new System.Windows.Forms.Padding(3);
			this.tabPeriodo.Size = new System.Drawing.Size(870, 437);
			this.tabPeriodo.TabIndex = 6;
			this.tabPeriodo.Text = "Periodo";
			this.tabPeriodo.UseVisualStyleBackColor = true;
			// 
			// btnActualizarPeriodo
			// 
			this.btnActualizarPeriodo.Location = new System.Drawing.Point(437, 358);
			this.btnActualizarPeriodo.Name = "btnActualizarPeriodo";
			this.btnActualizarPeriodo.Size = new System.Drawing.Size(75, 23);
			this.btnActualizarPeriodo.TabIndex = 13;
			this.btnActualizarPeriodo.Text = "Actualizar";
			this.btnActualizarPeriodo.UseVisualStyleBackColor = true;
			this.btnActualizarPeriodo.Click += new System.EventHandler(this.BtnActualizarPeriodoClick);
			// 
			// btnEliminarPeriodo
			// 
			this.btnEliminarPeriodo.Location = new System.Drawing.Point(334, 358);
			this.btnEliminarPeriodo.Name = "btnEliminarPeriodo";
			this.btnEliminarPeriodo.Size = new System.Drawing.Size(75, 23);
			this.btnEliminarPeriodo.TabIndex = 12;
			this.btnEliminarPeriodo.Text = "Eliminar";
			this.btnEliminarPeriodo.UseVisualStyleBackColor = true;
			this.btnEliminarPeriodo.Click += new System.EventHandler(this.BtnEliminarPeriodoClick);
			// 
			// txtIdPeriodo
			// 
			this.txtIdPeriodo.Location = new System.Drawing.Point(159, 358);
			this.txtIdPeriodo.Name = "txtIdPeriodo";
			this.txtIdPeriodo.Size = new System.Drawing.Size(138, 20);
			this.txtIdPeriodo.TabIndex = 10;
			// 
			// txtNombrePeriodo
			// 
			this.txtNombrePeriodo.Location = new System.Drawing.Point(160, 63);
			this.txtNombrePeriodo.Name = "txtNombrePeriodo";
			this.txtNombrePeriodo.Size = new System.Drawing.Size(200, 20);
			this.txtNombrePeriodo.TabIndex = 5;
			// 
			// lblIdPeriodo
			// 
			this.lblIdPeriodo.Location = new System.Drawing.Point(53, 358);
			this.lblIdPeriodo.Name = "lblIdPeriodo";
			this.lblIdPeriodo.Size = new System.Drawing.Size(100, 23);
			this.lblIdPeriodo.TabIndex = 9;
			this.lblIdPeriodo.Text = "ID del periodo";
			// 
			// tablaPeriodos
			// 
			this.tablaPeriodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaPeriodos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.idPeriodo,
			this.fechaInicio,
			this.fechaFin,
			this.nombrePeriodo});
			this.tablaPeriodos.Location = new System.Drawing.Point(89, 227);
			this.tablaPeriodos.Name = "tablaPeriodos";
			this.tablaPeriodos.Size = new System.Drawing.Size(443, 102);
			this.tablaPeriodos.TabIndex = 8;
			// 
			// idPeriodo
			// 
			this.idPeriodo.HeaderText = "ID";
			this.idPeriodo.Name = "idPeriodo";
			// 
			// fechaInicio
			// 
			this.fechaInicio.HeaderText = "Inicio";
			this.fechaInicio.Name = "fechaInicio";
			// 
			// fechaFin
			// 
			this.fechaFin.HeaderText = "Fin";
			this.fechaFin.Name = "fechaFin";
			// 
			// nombrePeriodo
			// 
			this.nombrePeriodo.HeaderText = "Nombre";
			this.nombrePeriodo.Name = "nombrePeriodo";
			// 
			// btnMostrarPeriodo
			// 
			this.btnMostrarPeriodo.Location = new System.Drawing.Point(378, 187);
			this.btnMostrarPeriodo.Name = "btnMostrarPeriodo";
			this.btnMostrarPeriodo.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarPeriodo.TabIndex = 7;
			this.btnMostrarPeriodo.Text = "Mostrar";
			this.btnMostrarPeriodo.UseVisualStyleBackColor = true;
			this.btnMostrarPeriodo.Click += new System.EventHandler(this.BtnMostrarPeriodoClick);
			// 
			// btnAgregarPeriodo
			// 
			this.btnAgregarPeriodo.Location = new System.Drawing.Point(143, 187);
			this.btnAgregarPeriodo.Name = "btnAgregarPeriodo";
			this.btnAgregarPeriodo.Size = new System.Drawing.Size(75, 23);
			this.btnAgregarPeriodo.TabIndex = 6;
			this.btnAgregarPeriodo.Text = "Agregar";
			this.btnAgregarPeriodo.UseVisualStyleBackColor = true;
			this.btnAgregarPeriodo.Click += new System.EventHandler(this.BtnAgregarPeriodoClick);
			// 
			// dateFinPeriodo
			// 
			this.dateFinPeriodo.CustomFormat = "yyyy-MM-dd";
			this.dateFinPeriodo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateFinPeriodo.Location = new System.Drawing.Point(160, 134);
			this.dateFinPeriodo.Name = "dateFinPeriodo";
			this.dateFinPeriodo.ShowCheckBox = true;
			this.dateFinPeriodo.Size = new System.Drawing.Size(200, 20);
			this.dateFinPeriodo.TabIndex = 4;
			// 
			// lblFinPeriodo
			// 
			this.lblFinPeriodo.Location = new System.Drawing.Point(53, 140);
			this.lblFinPeriodo.Name = "lblFinPeriodo";
			this.lblFinPeriodo.Size = new System.Drawing.Size(59, 23);
			this.lblFinPeriodo.TabIndex = 3;
			this.lblFinPeriodo.Text = "Fin";
			// 
			// dateInicioPeriodo
			// 
			this.dateInicioPeriodo.CustomFormat = "yyyy-MM-dd";
			this.dateInicioPeriodo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateInicioPeriodo.Location = new System.Drawing.Point(160, 93);
			this.dateInicioPeriodo.Name = "dateInicioPeriodo";
			this.dateInicioPeriodo.ShowCheckBox = true;
			this.dateInicioPeriodo.Size = new System.Drawing.Size(200, 20);
			this.dateInicioPeriodo.TabIndex = 2;
			this.dateInicioPeriodo.TabStop = false;
			// 
			// lblInicioPeriodo
			// 
			this.lblInicioPeriodo.Location = new System.Drawing.Point(53, 99);
			this.lblInicioPeriodo.Name = "lblInicioPeriodo";
			this.lblInicioPeriodo.Size = new System.Drawing.Size(59, 23);
			this.lblInicioPeriodo.TabIndex = 1;
			this.lblInicioPeriodo.Text = "Inicio";
			// 
			// lblNombrePeriodo
			// 
			this.lblNombrePeriodo.Location = new System.Drawing.Point(53, 66);
			this.lblNombrePeriodo.Name = "lblNombrePeriodo";
			this.lblNombrePeriodo.Size = new System.Drawing.Size(59, 23);
			this.lblNombrePeriodo.TabIndex = 0;
			this.lblNombrePeriodo.Text = "Nombre";
			// 
			// tabProfesor
			// 
			this.tabProfesor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabProfesor.BackgroundImage")));
			this.tabProfesor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabProfesor.Controls.Add(this.btnActualizarProfesor);
			this.tabProfesor.Controls.Add(this.lblBuscarProfesor);
			this.tabProfesor.Controls.Add(this.txtBuscarProfesor);
			this.tabProfesor.Controls.Add(this.btnBuscarProfesor);
			this.tabProfesor.Controls.Add(this.btnEliminarProfesor);
			this.tabProfesor.Controls.Add(this.btnMostrarProfesores);
			this.tabProfesor.Controls.Add(this.tablaProfesores);
			this.tabProfesor.Controls.Add(this.btnAgregarProfesor);
			this.tabProfesor.Controls.Add(this.boxCarreraProfesor);
			this.tabProfesor.Controls.Add(this.txtTelProfesor);
			this.tabProfesor.Controls.Add(this.txtMail);
			this.tabProfesor.Controls.Add(this.txtAMprofesor);
			this.tabProfesor.Controls.Add(this.txtAPprofesor);
			this.tabProfesor.Controls.Add(this.txtNombreProfesor);
			this.tabProfesor.Controls.Add(this.txtCodigoProfesor);
			this.tabProfesor.Controls.Add(this.lblCarreraProfesor);
			this.tabProfesor.Controls.Add(this.lblTelefonoProfesor);
			this.tabProfesor.Controls.Add(this.lblCorreoProfesor);
			this.tabProfesor.Controls.Add(this.lblApellidoM);
			this.tabProfesor.Controls.Add(this.lblApellidoP);
			this.tabProfesor.Controls.Add(this.lblNombreProfesor);
			this.tabProfesor.Controls.Add(this.lblCodigoProfesor);
			this.tabProfesor.Location = new System.Drawing.Point(4, 22);
			this.tabProfesor.Name = "tabProfesor";
			this.tabProfesor.Padding = new System.Windows.Forms.Padding(3);
			this.tabProfesor.Size = new System.Drawing.Size(870, 437);
			this.tabProfesor.TabIndex = 5;
			this.tabProfesor.Text = "Profesor";
			this.tabProfesor.UseVisualStyleBackColor = true;
			// 
			// btnActualizarProfesor
			// 
			this.btnActualizarProfesor.Location = new System.Drawing.Point(378, 277);
			this.btnActualizarProfesor.Name = "btnActualizarProfesor";
			this.btnActualizarProfesor.Size = new System.Drawing.Size(75, 23);
			this.btnActualizarProfesor.TabIndex = 21;
			this.btnActualizarProfesor.Text = "Actualizar";
			this.btnActualizarProfesor.UseVisualStyleBackColor = true;
			this.btnActualizarProfesor.Click += new System.EventHandler(this.BtnActualizarProfesorClick);
			// 
			// lblBuscarProfesor
			// 
			this.lblBuscarProfesor.Location = new System.Drawing.Point(475, 277);
			this.lblBuscarProfesor.Name = "lblBuscarProfesor";
			this.lblBuscarProfesor.Size = new System.Drawing.Size(44, 23);
			this.lblBuscarProfesor.TabIndex = 20;
			this.lblBuscarProfesor.Text = "Codigo:";
			// 
			// txtBuscarProfesor
			// 
			this.txtBuscarProfesor.Location = new System.Drawing.Point(525, 280);
			this.txtBuscarProfesor.Name = "txtBuscarProfesor";
			this.txtBuscarProfesor.Size = new System.Drawing.Size(100, 20);
			this.txtBuscarProfesor.TabIndex = 19;
			// 
			// btnBuscarProfesor
			// 
			this.btnBuscarProfesor.Location = new System.Drawing.Point(206, 277);
			this.btnBuscarProfesor.Name = "btnBuscarProfesor";
			this.btnBuscarProfesor.Size = new System.Drawing.Size(75, 23);
			this.btnBuscarProfesor.TabIndex = 18;
			this.btnBuscarProfesor.Text = "Buscar";
			this.btnBuscarProfesor.UseVisualStyleBackColor = true;
			this.btnBuscarProfesor.Click += new System.EventHandler(this.BtnBuscarProfesorClick);
			// 
			// btnEliminarProfesor
			// 
			this.btnEliminarProfesor.Location = new System.Drawing.Point(297, 277);
			this.btnEliminarProfesor.Name = "btnEliminarProfesor";
			this.btnEliminarProfesor.Size = new System.Drawing.Size(75, 23);
			this.btnEliminarProfesor.TabIndex = 17;
			this.btnEliminarProfesor.Text = "Eliminar";
			this.btnEliminarProfesor.UseVisualStyleBackColor = true;
			this.btnEliminarProfesor.Click += new System.EventHandler(this.BtnEliminarProfesorClick);
			// 
			// btnMostrarProfesores
			// 
			this.btnMostrarProfesores.Location = new System.Drawing.Point(116, 277);
			this.btnMostrarProfesores.Name = "btnMostrarProfesores";
			this.btnMostrarProfesores.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarProfesores.TabIndex = 16;
			this.btnMostrarProfesores.Text = "Mostrar";
			this.btnMostrarProfesores.UseVisualStyleBackColor = true;
			this.btnMostrarProfesores.Click += new System.EventHandler(this.BtnMostrarProfesoresClick);
			// 
			// tablaProfesores
			// 
			this.tablaProfesores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tablaProfesores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaProfesores.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.tablaProfesores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaProfesores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoProfesor,
			this.nombreProfesor,
			this.aPaternoProfesor,
			this.aMaternoProfesor,
			this.correoProfesor,
			this.telefonoProfesor,
			this.carreraProfesor});
			this.tablaProfesores.Location = new System.Drawing.Point(315, 42);
			this.tablaProfesores.Name = "tablaProfesores";
			this.tablaProfesores.Size = new System.Drawing.Size(549, 123);
			this.tablaProfesores.TabIndex = 15;
			this.tablaProfesores.Visible = false;
			// 
			// codigoProfesor
			// 
			this.codigoProfesor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.codigoProfesor.HeaderText = "Codigo";
			this.codigoProfesor.Name = "codigoProfesor";
			// 
			// nombreProfesor
			// 
			this.nombreProfesor.HeaderText = "Nombre";
			this.nombreProfesor.Name = "nombreProfesor";
			// 
			// aPaternoProfesor
			// 
			this.aPaternoProfesor.HeaderText = "Apellido Paterno";
			this.aPaternoProfesor.Name = "aPaternoProfesor";
			// 
			// aMaternoProfesor
			// 
			this.aMaternoProfesor.HeaderText = "Apellido Materno";
			this.aMaternoProfesor.Name = "aMaternoProfesor";
			// 
			// correoProfesor
			// 
			this.correoProfesor.HeaderText = "Correo";
			this.correoProfesor.Name = "correoProfesor";
			// 
			// telefonoProfesor
			// 
			this.telefonoProfesor.HeaderText = "Telefono";
			this.telefonoProfesor.Name = "telefonoProfesor";
			// 
			// carreraProfesor
			// 
			this.carreraProfesor.HeaderText = "Carrera";
			this.carreraProfesor.Name = "carreraProfesor";
			// 
			// btnAgregarProfesor
			// 
			this.btnAgregarProfesor.Location = new System.Drawing.Point(24, 277);
			this.btnAgregarProfesor.Name = "btnAgregarProfesor";
			this.btnAgregarProfesor.Size = new System.Drawing.Size(75, 23);
			this.btnAgregarProfesor.TabIndex = 13;
			this.btnAgregarProfesor.Text = "Agregar";
			this.btnAgregarProfesor.UseVisualStyleBackColor = true;
			this.btnAgregarProfesor.Click += new System.EventHandler(this.BtnAgregarProfesorClick);
			// 
			// boxCarreraProfesor
			// 
			this.boxCarreraProfesor.FormattingEnabled = true;
			this.boxCarreraProfesor.Location = new System.Drawing.Point(130, 238);
			this.boxCarreraProfesor.Name = "boxCarreraProfesor";
			this.boxCarreraProfesor.Size = new System.Drawing.Size(121, 21);
			this.boxCarreraProfesor.TabIndex = 12;
			// 
			// txtTelProfesor
			// 
			this.txtTelProfesor.Location = new System.Drawing.Point(130, 205);
			this.txtTelProfesor.Name = "txtTelProfesor";
			this.txtTelProfesor.Size = new System.Drawing.Size(180, 20);
			this.txtTelProfesor.TabIndex = 11;
			// 
			// txtMail
			// 
			this.txtMail.Location = new System.Drawing.Point(130, 168);
			this.txtMail.Name = "txtMail";
			this.txtMail.Size = new System.Drawing.Size(179, 20);
			this.txtMail.TabIndex = 10;
			// 
			// txtAMprofesor
			// 
			this.txtAMprofesor.Location = new System.Drawing.Point(130, 139);
			this.txtAMprofesor.Name = "txtAMprofesor";
			this.txtAMprofesor.Size = new System.Drawing.Size(179, 20);
			this.txtAMprofesor.TabIndex = 9;
			// 
			// txtAPprofesor
			// 
			this.txtAPprofesor.Location = new System.Drawing.Point(130, 110);
			this.txtAPprofesor.Name = "txtAPprofesor";
			this.txtAPprofesor.Size = new System.Drawing.Size(179, 20);
			this.txtAPprofesor.TabIndex = 8;
			// 
			// txtNombreProfesor
			// 
			this.txtNombreProfesor.Location = new System.Drawing.Point(130, 77);
			this.txtNombreProfesor.Name = "txtNombreProfesor";
			this.txtNombreProfesor.Size = new System.Drawing.Size(179, 20);
			this.txtNombreProfesor.TabIndex = 7;
			// 
			// txtCodigoProfesor
			// 
			this.txtCodigoProfesor.Location = new System.Drawing.Point(130, 42);
			this.txtCodigoProfesor.Name = "txtCodigoProfesor";
			this.txtCodigoProfesor.Size = new System.Drawing.Size(179, 20);
			this.txtCodigoProfesor.TabIndex = 6;
			// 
			// lblCarreraProfesor
			// 
			this.lblCarreraProfesor.Location = new System.Drawing.Point(24, 236);
			this.lblCarreraProfesor.Name = "lblCarreraProfesor";
			this.lblCarreraProfesor.Size = new System.Drawing.Size(100, 23);
			this.lblCarreraProfesor.TabIndex = 5;
			this.lblCarreraProfesor.Text = "Carrera:";
			// 
			// lblTelefonoProfesor
			// 
			this.lblTelefonoProfesor.Location = new System.Drawing.Point(24, 208);
			this.lblTelefonoProfesor.Name = "lblTelefonoProfesor";
			this.lblTelefonoProfesor.Size = new System.Drawing.Size(100, 23);
			this.lblTelefonoProfesor.TabIndex = 4;
			this.lblTelefonoProfesor.Text = "Telefono:";
			// 
			// lblCorreoProfesor
			// 
			this.lblCorreoProfesor.Location = new System.Drawing.Point(24, 171);
			this.lblCorreoProfesor.Name = "lblCorreoProfesor";
			this.lblCorreoProfesor.Size = new System.Drawing.Size(100, 23);
			this.lblCorreoProfesor.TabIndex = 1;
			this.lblCorreoProfesor.Text = "e-mail:";
			// 
			// lblApellidoM
			// 
			this.lblApellidoM.Location = new System.Drawing.Point(24, 142);
			this.lblApellidoM.Name = "lblApellidoM";
			this.lblApellidoM.Size = new System.Drawing.Size(100, 23);
			this.lblApellidoM.TabIndex = 3;
			this.lblApellidoM.Text = "Apellido Materno:";
			// 
			// lblApellidoP
			// 
			this.lblApellidoP.Location = new System.Drawing.Point(24, 110);
			this.lblApellidoP.Name = "lblApellidoP";
			this.lblApellidoP.Size = new System.Drawing.Size(100, 23);
			this.lblApellidoP.TabIndex = 2;
			this.lblApellidoP.Text = "Apellido Paterno:";
			// 
			// lblNombreProfesor
			// 
			this.lblNombreProfesor.Location = new System.Drawing.Point(24, 77);
			this.lblNombreProfesor.Name = "lblNombreProfesor";
			this.lblNombreProfesor.Size = new System.Drawing.Size(100, 23);
			this.lblNombreProfesor.TabIndex = 1;
			this.lblNombreProfesor.Text = "Nombre:";
			// 
			// lblCodigoProfesor
			// 
			this.lblCodigoProfesor.Location = new System.Drawing.Point(24, 45);
			this.lblCodigoProfesor.Name = "lblCodigoProfesor";
			this.lblCodigoProfesor.Size = new System.Drawing.Size(100, 23);
			this.lblCodigoProfesor.TabIndex = 0;
			this.lblCodigoProfesor.Text = "Codigo:";
			// 
			// tabAsignatura
			// 
			this.tabAsignatura.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabAsignatura.BackgroundImage")));
			this.tabAsignatura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabAsignatura.Controls.Add(this.btnActualizarAsignatura);
			this.tabAsignatura.Controls.Add(this.btnEliminar);
			this.tabAsignatura.Controls.Add(this.lblBusquedaAsignatura);
			this.tabAsignatura.Controls.Add(this.txtBuscarAsignatura);
			this.tabAsignatura.Controls.Add(this.btnBuscarAsignatura);
			this.tabAsignatura.Controls.Add(this.btnMostarAsignatura);
			this.tabAsignatura.Controls.Add(this.tablaAsignatura);
			this.tabAsignatura.Controls.Add(this.btnVincularAsignatura);
			this.tabAsignatura.Controls.Add(this.btnAddAsignatura);
			this.tabAsignatura.Controls.Add(this.txtCodigoAsignatura);
			this.tabAsignatura.Controls.Add(this.boxGradoAsignatura);
			this.tabAsignatura.Controls.Add(this.boxCarreraAsignatura);
			this.tabAsignatura.Controls.Add(this.lblCodigoAsignatura);
			this.tabAsignatura.Controls.Add(this.txtCreditosAsignatura);
			this.tabAsignatura.Controls.Add(this.txtNombreAsignatura);
			this.tabAsignatura.Controls.Add(this.lblVinculo);
			this.tabAsignatura.Controls.Add(this.lblCreditosAsignatura);
			this.tabAsignatura.Controls.Add(this.lblNombreAsignatura);
			this.tabAsignatura.Location = new System.Drawing.Point(4, 22);
			this.tabAsignatura.Name = "tabAsignatura";
			this.tabAsignatura.Padding = new System.Windows.Forms.Padding(3);
			this.tabAsignatura.Size = new System.Drawing.Size(870, 437);
			this.tabAsignatura.TabIndex = 3;
			this.tabAsignatura.Text = "Asignatura";
			this.tabAsignatura.UseVisualStyleBackColor = true;
			// 
			// btnActualizarAsignatura
			// 
			this.btnActualizarAsignatura.Location = new System.Drawing.Point(321, 381);
			this.btnActualizarAsignatura.Name = "btnActualizarAsignatura";
			this.btnActualizarAsignatura.Size = new System.Drawing.Size(75, 23);
			this.btnActualizarAsignatura.TabIndex = 17;
			this.btnActualizarAsignatura.Text = "Actualizar";
			this.btnActualizarAsignatura.UseVisualStyleBackColor = true;
			this.btnActualizarAsignatura.Click += new System.EventHandler(this.BtnActualizarAsignaturaClick);
			// 
			// btnEliminar
			// 
			this.btnEliminar.Location = new System.Drawing.Point(321, 350);
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(75, 23);
			this.btnEliminar.TabIndex = 16;
			this.btnEliminar.Text = "Eliminar";
			this.btnEliminar.UseVisualStyleBackColor = true;
			this.btnEliminar.Click += new System.EventHandler(this.BtnEliminarClick);
			// 
			// lblBusquedaAsignatura
			// 
			this.lblBusquedaAsignatura.Location = new System.Drawing.Point(83, 353);
			this.lblBusquedaAsignatura.Name = "lblBusquedaAsignatura";
			this.lblBusquedaAsignatura.Size = new System.Drawing.Size(45, 20);
			this.lblBusquedaAsignatura.TabIndex = 15;
			this.lblBusquedaAsignatura.Text = "Codigo";
			// 
			// txtBuscarAsignatura
			// 
			this.txtBuscarAsignatura.Location = new System.Drawing.Point(134, 353);
			this.txtBuscarAsignatura.Name = "txtBuscarAsignatura";
			this.txtBuscarAsignatura.Size = new System.Drawing.Size(142, 20);
			this.txtBuscarAsignatura.TabIndex = 14;
			// 
			// btnBuscarAsignatura
			// 
			this.btnBuscarAsignatura.Location = new System.Drawing.Point(321, 320);
			this.btnBuscarAsignatura.Name = "btnBuscarAsignatura";
			this.btnBuscarAsignatura.Size = new System.Drawing.Size(75, 23);
			this.btnBuscarAsignatura.TabIndex = 13;
			this.btnBuscarAsignatura.Text = "buscar";
			this.btnBuscarAsignatura.UseVisualStyleBackColor = true;
			this.btnBuscarAsignatura.Click += new System.EventHandler(this.BtnBuscarAsignaturaClick);
			// 
			// btnMostarAsignatura
			// 
			this.btnMostarAsignatura.Location = new System.Drawing.Point(358, 159);
			this.btnMostarAsignatura.Name = "btnMostarAsignatura";
			this.btnMostarAsignatura.Size = new System.Drawing.Size(75, 23);
			this.btnMostarAsignatura.TabIndex = 12;
			this.btnMostarAsignatura.Text = "Mostrar";
			this.btnMostarAsignatura.UseVisualStyleBackColor = true;
			this.btnMostarAsignatura.Click += new System.EventHandler(this.BtnMostarAsignaturaClick);
			// 
			// tablaAsignatura
			// 
			this.tablaAsignatura.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tablaAsignatura.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaAsignatura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaAsignatura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoAsignatura,
			this.nombreAsignatura,
			this.creditosAsignatura,
			this.carreraAsignatrura,
			this.gradoAsignatura});
			this.tablaAsignatura.Location = new System.Drawing.Point(69, 188);
			this.tablaAsignatura.Name = "tablaAsignatura";
			this.tablaAsignatura.Size = new System.Drawing.Size(660, 118);
			this.tablaAsignatura.TabIndex = 11;
			// 
			// codigoAsignatura
			// 
			this.codigoAsignatura.HeaderText = "Codigo";
			this.codigoAsignatura.Name = "codigoAsignatura";
			// 
			// nombreAsignatura
			// 
			this.nombreAsignatura.HeaderText = "Asignatura";
			this.nombreAsignatura.Name = "nombreAsignatura";
			// 
			// creditosAsignatura
			// 
			this.creditosAsignatura.HeaderText = "Creditos";
			this.creditosAsignatura.Name = "creditosAsignatura";
			// 
			// carreraAsignatrura
			// 
			this.carreraAsignatrura.HeaderText = "Carrera";
			this.carreraAsignatrura.Name = "carreraAsignatrura";
			// 
			// gradoAsignatura
			// 
			this.gradoAsignatura.HeaderText = "Grado";
			this.gradoAsignatura.Name = "gradoAsignatura";
			// 
			// btnVincularAsignatura
			// 
			this.btnVincularAsignatura.Location = new System.Drawing.Point(670, 68);
			this.btnVincularAsignatura.Name = "btnVincularAsignatura";
			this.btnVincularAsignatura.Size = new System.Drawing.Size(75, 23);
			this.btnVincularAsignatura.TabIndex = 10;
			this.btnVincularAsignatura.Text = "Vincular";
			this.btnVincularAsignatura.UseVisualStyleBackColor = true;
			this.btnVincularAsignatura.Click += new System.EventHandler(this.BtnVincularAsignaturaClick);
			// 
			// btnAddAsignatura
			// 
			this.btnAddAsignatura.Location = new System.Drawing.Point(367, 65);
			this.btnAddAsignatura.Name = "btnAddAsignatura";
			this.btnAddAsignatura.Size = new System.Drawing.Size(75, 23);
			this.btnAddAsignatura.TabIndex = 9;
			this.btnAddAsignatura.Text = "Agregar";
			this.btnAddAsignatura.UseVisualStyleBackColor = true;
			this.btnAddAsignatura.Click += new System.EventHandler(this.BtnAddAsignaturaClick);
			// 
			// txtCodigoAsignatura
			// 
			this.txtCodigoAsignatura.Location = new System.Drawing.Point(189, 38);
			this.txtCodigoAsignatura.Name = "txtCodigoAsignatura";
			this.txtCodigoAsignatura.Size = new System.Drawing.Size(149, 20);
			this.txtCodigoAsignatura.TabIndex = 8;
			// 
			// boxGradoAsignatura
			// 
			this.boxGradoAsignatura.FormattingEnabled = true;
			this.boxGradoAsignatura.Location = new System.Drawing.Point(470, 92);
			this.boxGradoAsignatura.Name = "boxGradoAsignatura";
			this.boxGradoAsignatura.Size = new System.Drawing.Size(167, 21);
			this.boxGradoAsignatura.TabIndex = 7;
			// 
			// boxCarreraAsignatura
			// 
			this.boxCarreraAsignatura.FormattingEnabled = true;
			this.boxCarreraAsignatura.Location = new System.Drawing.Point(470, 65);
			this.boxCarreraAsignatura.Name = "boxCarreraAsignatura";
			this.boxCarreraAsignatura.Size = new System.Drawing.Size(167, 21);
			this.boxCarreraAsignatura.TabIndex = 6;
			// 
			// lblCodigoAsignatura
			// 
			this.lblCodigoAsignatura.Location = new System.Drawing.Point(30, 41);
			this.lblCodigoAsignatura.Name = "lblCodigoAsignatura";
			this.lblCodigoAsignatura.Size = new System.Drawing.Size(100, 23);
			this.lblCodigoAsignatura.TabIndex = 5;
			this.lblCodigoAsignatura.Text = "Codigo";
			// 
			// txtCreditosAsignatura
			// 
			this.txtCreditosAsignatura.Location = new System.Drawing.Point(189, 105);
			this.txtCreditosAsignatura.Name = "txtCreditosAsignatura";
			this.txtCreditosAsignatura.Size = new System.Drawing.Size(149, 20);
			this.txtCreditosAsignatura.TabIndex = 4;
			// 
			// txtNombreAsignatura
			// 
			this.txtNombreAsignatura.Location = new System.Drawing.Point(189, 70);
			this.txtNombreAsignatura.Name = "txtNombreAsignatura";
			this.txtNombreAsignatura.Size = new System.Drawing.Size(149, 20);
			this.txtNombreAsignatura.TabIndex = 3;
			// 
			// lblVinculo
			// 
			this.lblVinculo.Location = new System.Drawing.Point(468, 37);
			this.lblVinculo.Name = "lblVinculo";
			this.lblVinculo.Size = new System.Drawing.Size(169, 27);
			this.lblVinculo.TabIndex = 2;
			this.lblVinculo.Text = "Vinculacion con carrera y grado";
			// 
			// lblCreditosAsignatura
			// 
			this.lblCreditosAsignatura.Location = new System.Drawing.Point(30, 106);
			this.lblCreditosAsignatura.Name = "lblCreditosAsignatura";
			this.lblCreditosAsignatura.Size = new System.Drawing.Size(100, 23);
			this.lblCreditosAsignatura.TabIndex = 1;
			this.lblCreditosAsignatura.Text = "Creditos";
			// 
			// lblNombreAsignatura
			// 
			this.lblNombreAsignatura.Location = new System.Drawing.Point(30, 73);
			this.lblNombreAsignatura.Name = "lblNombreAsignatura";
			this.lblNombreAsignatura.Size = new System.Drawing.Size(126, 25);
			this.lblNombreAsignatura.TabIndex = 0;
			this.lblNombreAsignatura.Text = "Nombre de la asignatura";
			// 
			// tabAlumno
			// 
			this.tabAlumno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabAlumno.BackgroundImage")));
			this.tabAlumno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabAlumno.Controls.Add(this.label4);
			this.tabAlumno.Controls.Add(this.txtBuscarAlumno);
			this.tabAlumno.Controls.Add(this.btnActualizarAlumno);
			this.tabAlumno.Controls.Add(this.btnAlumnoBuscar);
			this.tabAlumno.Controls.Add(this.btnEliminarAlumno);
			this.tabAlumno.Controls.Add(this.label3);
			this.tabAlumno.Controls.Add(this.comboBoxAlumnoGrupo);
			this.tabAlumno.Controls.Add(this.dataGridAlumno);
			this.tabAlumno.Controls.Add(this.btnMostrarAlumno);
			this.tabAlumno.Controls.Add(this.btnAgregarAlumno);
			this.tabAlumno.Controls.Add(this.txtAMalumno);
			this.tabAlumno.Controls.Add(this.txtAPalumno);
			this.tabAlumno.Controls.Add(this.txtNombreAlumno);
			this.tabAlumno.Controls.Add(this.txtCodigoAlumno);
			this.tabAlumno.Controls.Add(this.labelAMalumno);
			this.tabAlumno.Controls.Add(this.lblAPalumno);
			this.tabAlumno.Controls.Add(this.lblNombreAlumno);
			this.tabAlumno.Controls.Add(this.lblAlumnoCodigo);
			this.tabAlumno.Location = new System.Drawing.Point(4, 22);
			this.tabAlumno.Name = "tabAlumno";
			this.tabAlumno.Padding = new System.Windows.Forms.Padding(3);
			this.tabAlumno.Size = new System.Drawing.Size(870, 437);
			this.tabAlumno.TabIndex = 2;
			this.tabAlumno.Text = "Alumno";
			this.tabAlumno.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(443, 218);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(44, 23);
			this.label4.TabIndex = 34;
			this.label4.Text = "Codigo:";
			// 
			// txtBuscarAlumno
			// 
			this.txtBuscarAlumno.Location = new System.Drawing.Point(493, 221);
			this.txtBuscarAlumno.Name = "txtBuscarAlumno";
			this.txtBuscarAlumno.Size = new System.Drawing.Size(100, 20);
			this.txtBuscarAlumno.TabIndex = 33;
			// 
			// btnActualizarAlumno
			// 
			this.btnActualizarAlumno.Location = new System.Drawing.Point(358, 218);
			this.btnActualizarAlumno.Name = "btnActualizarAlumno";
			this.btnActualizarAlumno.Size = new System.Drawing.Size(75, 23);
			this.btnActualizarAlumno.TabIndex = 32;
			this.btnActualizarAlumno.Text = "Actualizar";
			this.btnActualizarAlumno.UseVisualStyleBackColor = true;
			this.btnActualizarAlumno.Click += new System.EventHandler(this.BtnActualizarAlumnoClick);
			// 
			// btnAlumnoBuscar
			// 
			this.btnAlumnoBuscar.Location = new System.Drawing.Point(186, 218);
			this.btnAlumnoBuscar.Name = "btnAlumnoBuscar";
			this.btnAlumnoBuscar.Size = new System.Drawing.Size(75, 23);
			this.btnAlumnoBuscar.TabIndex = 31;
			this.btnAlumnoBuscar.Text = "Buscar";
			this.btnAlumnoBuscar.UseVisualStyleBackColor = true;
			this.btnAlumnoBuscar.Click += new System.EventHandler(this.Button2Click);
			// 
			// btnEliminarAlumno
			// 
			this.btnEliminarAlumno.Location = new System.Drawing.Point(277, 218);
			this.btnEliminarAlumno.Name = "btnEliminarAlumno";
			this.btnEliminarAlumno.Size = new System.Drawing.Size(75, 23);
			this.btnEliminarAlumno.TabIndex = 30;
			this.btnEliminarAlumno.Text = "Eliminar";
			this.btnEliminarAlumno.UseVisualStyleBackColor = true;
			this.btnEliminarAlumno.Click += new System.EventHandler(this.BtnEliminarAlumnoClick);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(14, 169);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 29;
			this.label3.Text = "Grupo ";
			// 
			// comboBoxAlumnoGrupo
			// 
			this.comboBoxAlumnoGrupo.FormattingEnabled = true;
			this.comboBoxAlumnoGrupo.Location = new System.Drawing.Point(120, 171);
			this.comboBoxAlumnoGrupo.Name = "comboBoxAlumnoGrupo";
			this.comboBoxAlumnoGrupo.Size = new System.Drawing.Size(178, 21);
			this.comboBoxAlumnoGrupo.TabIndex = 28;
			// 
			// dataGridAlumno
			// 
			this.dataGridAlumno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridAlumno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoAlumno,
			this.nombreAlumno,
			this.aPaternoAlumno,
			this.aMaternoAlumno,
			this.grupoAlumno,
			this.gradoAlumno});
			this.dataGridAlumno.Location = new System.Drawing.Point(319, 21);
			this.dataGridAlumno.Name = "dataGridAlumno";
			this.dataGridAlumno.Size = new System.Drawing.Size(548, 150);
			this.dataGridAlumno.TabIndex = 27;
			this.dataGridAlumno.Visible = false;
			// 
			// codigoAlumno
			// 
			this.codigoAlumno.HeaderText = "Codigo";
			this.codigoAlumno.Name = "codigoAlumno";
			// 
			// nombreAlumno
			// 
			this.nombreAlumno.HeaderText = "Nombre";
			this.nombreAlumno.Name = "nombreAlumno";
			// 
			// aPaternoAlumno
			// 
			this.aPaternoAlumno.HeaderText = "Apellido Paterno";
			this.aPaternoAlumno.Name = "aPaternoAlumno";
			// 
			// aMaternoAlumno
			// 
			this.aMaternoAlumno.HeaderText = "Apellido Materno";
			this.aMaternoAlumno.Name = "aMaternoAlumno";
			// 
			// grupoAlumno
			// 
			this.grupoAlumno.HeaderText = "Grupo";
			this.grupoAlumno.Name = "grupoAlumno";
			// 
			// gradoAlumno
			// 
			this.gradoAlumno.HeaderText = "Grado";
			this.gradoAlumno.Name = "gradoAlumno";
			// 
			// btnMostrarAlumno
			// 
			this.btnMostrarAlumno.Location = new System.Drawing.Point(105, 218);
			this.btnMostrarAlumno.Name = "btnMostrarAlumno";
			this.btnMostrarAlumno.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarAlumno.TabIndex = 23;
			this.btnMostrarAlumno.Text = "Mostrar";
			this.btnMostrarAlumno.UseVisualStyleBackColor = true;
			this.btnMostrarAlumno.Click += new System.EventHandler(this.BtnMostrarAlumnoClick);
			// 
			// btnAgregarAlumno
			// 
			this.btnAgregarAlumno.Location = new System.Drawing.Point(13, 218);
			this.btnAgregarAlumno.Name = "btnAgregarAlumno";
			this.btnAgregarAlumno.Size = new System.Drawing.Size(75, 23);
			this.btnAgregarAlumno.TabIndex = 22;
			this.btnAgregarAlumno.Text = "Agregar";
			this.btnAgregarAlumno.UseVisualStyleBackColor = true;
			this.btnAgregarAlumno.Click += new System.EventHandler(this.BtnAgregarAlumnoClick);
			// 
			// txtAMalumno
			// 
			this.txtAMalumno.Location = new System.Drawing.Point(119, 128);
			this.txtAMalumno.Name = "txtAMalumno";
			this.txtAMalumno.Size = new System.Drawing.Size(179, 20);
			this.txtAMalumno.TabIndex = 17;
			// 
			// txtAPalumno
			// 
			this.txtAPalumno.Location = new System.Drawing.Point(119, 99);
			this.txtAPalumno.Name = "txtAPalumno";
			this.txtAPalumno.Size = new System.Drawing.Size(179, 20);
			this.txtAPalumno.TabIndex = 16;
			// 
			// txtNombreAlumno
			// 
			this.txtNombreAlumno.Location = new System.Drawing.Point(119, 66);
			this.txtNombreAlumno.Name = "txtNombreAlumno";
			this.txtNombreAlumno.Size = new System.Drawing.Size(179, 20);
			this.txtNombreAlumno.TabIndex = 15;
			// 
			// txtCodigoAlumno
			// 
			this.txtCodigoAlumno.Location = new System.Drawing.Point(119, 31);
			this.txtCodigoAlumno.Name = "txtCodigoAlumno";
			this.txtCodigoAlumno.Size = new System.Drawing.Size(179, 20);
			this.txtCodigoAlumno.TabIndex = 14;
			// 
			// labelAMalumno
			// 
			this.labelAMalumno.Location = new System.Drawing.Point(13, 131);
			this.labelAMalumno.Name = "labelAMalumno";
			this.labelAMalumno.Size = new System.Drawing.Size(100, 23);
			this.labelAMalumno.TabIndex = 13;
			this.labelAMalumno.Text = "Apellido Materno:";
			// 
			// lblAPalumno
			// 
			this.lblAPalumno.Location = new System.Drawing.Point(13, 99);
			this.lblAPalumno.Name = "lblAPalumno";
			this.lblAPalumno.Size = new System.Drawing.Size(100, 23);
			this.lblAPalumno.TabIndex = 12;
			this.lblAPalumno.Text = "Apellido Paterno:";
			// 
			// lblNombreAlumno
			// 
			this.lblNombreAlumno.Location = new System.Drawing.Point(13, 66);
			this.lblNombreAlumno.Name = "lblNombreAlumno";
			this.lblNombreAlumno.Size = new System.Drawing.Size(100, 23);
			this.lblNombreAlumno.TabIndex = 11;
			this.lblNombreAlumno.Text = "Nombre:";
			// 
			// lblAlumnoCodigo
			// 
			this.lblAlumnoCodigo.Location = new System.Drawing.Point(13, 34);
			this.lblAlumnoCodigo.Name = "lblAlumnoCodigo";
			this.lblAlumnoCodigo.Size = new System.Drawing.Size(100, 23);
			this.lblAlumnoCodigo.TabIndex = 10;
			this.lblAlumnoCodigo.Text = "Codigo:";
			// 
			// tabCarrera
			// 
			this.tabCarrera.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabCarrera.BackgroundImage")));
			this.tabCarrera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabCarrera.Controls.Add(this.btnUpdateCarrera);
			this.tabCarrera.Controls.Add(this.btnEliminarCarrera);
			this.tabCarrera.Controls.Add(this.lblNombreAddCarrera);
			this.tabCarrera.Controls.Add(this.btnSearchCarrera);
			this.tabCarrera.Controls.Add(this.txtSearchCarrera);
			this.tabCarrera.Controls.Add(this.txtNombreCarrera);
			this.tabCarrera.Controls.Add(this.txtAreaCarrera);
			this.tabCarrera.Controls.Add(this.txtNivelCarrera);
			this.tabCarrera.Controls.Add(this.lblAreaAddCarrera);
			this.tabCarrera.Controls.Add(this.lblSearchCarrera);
			this.tabCarrera.Controls.Add(this.tablaCarreras);
			this.tabCarrera.Controls.Add(this.btnViewCarrera);
			this.tabCarrera.Controls.Add(this.lblNivelAddCarrera);
			this.tabCarrera.Controls.Add(this.btnAddCarrera);
			this.tabCarrera.Location = new System.Drawing.Point(4, 22);
			this.tabCarrera.Name = "tabCarrera";
			this.tabCarrera.Padding = new System.Windows.Forms.Padding(3);
			this.tabCarrera.Size = new System.Drawing.Size(870, 437);
			this.tabCarrera.TabIndex = 0;
			this.tabCarrera.Text = "Carrera";
			this.tabCarrera.UseVisualStyleBackColor = true;
			// 
			// btnUpdateCarrera
			// 
			this.btnUpdateCarrera.Location = new System.Drawing.Point(532, 364);
			this.btnUpdateCarrera.Name = "btnUpdateCarrera";
			this.btnUpdateCarrera.Size = new System.Drawing.Size(96, 26);
			this.btnUpdateCarrera.TabIndex = 13;
			this.btnUpdateCarrera.Text = "Actualizar";
			this.btnUpdateCarrera.UseVisualStyleBackColor = true;
			this.btnUpdateCarrera.Click += new System.EventHandler(this.BtnUpdateCarreraClick);
			// 
			// btnEliminarCarrera
			// 
			this.btnEliminarCarrera.Location = new System.Drawing.Point(422, 364);
			this.btnEliminarCarrera.Name = "btnEliminarCarrera";
			this.btnEliminarCarrera.Size = new System.Drawing.Size(83, 24);
			this.btnEliminarCarrera.TabIndex = 12;
			this.btnEliminarCarrera.Text = "Eliminar";
			this.btnEliminarCarrera.UseVisualStyleBackColor = true;
			this.btnEliminarCarrera.Click += new System.EventHandler(this.BtnEliminarCarreraClick);
			// 
			// lblNombreAddCarrera
			// 
			this.lblNombreAddCarrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblNombreAddCarrera.ForeColor = System.Drawing.Color.Black;
			this.lblNombreAddCarrera.Location = new System.Drawing.Point(216, 44);
			this.lblNombreAddCarrera.Name = "lblNombreAddCarrera";
			this.lblNombreAddCarrera.Size = new System.Drawing.Size(63, 23);
			this.lblNombreAddCarrera.TabIndex = 0;
			this.lblNombreAddCarrera.Text = "Nombre";
			// 
			// btnSearchCarrera
			// 
			this.btnSearchCarrera.Location = new System.Drawing.Point(319, 364);
			this.btnSearchCarrera.Name = "btnSearchCarrera";
			this.btnSearchCarrera.Size = new System.Drawing.Size(75, 23);
			this.btnSearchCarrera.TabIndex = 10;
			this.btnSearchCarrera.Text = "Buscar";
			this.btnSearchCarrera.UseVisualStyleBackColor = true;
			this.btnSearchCarrera.Click += new System.EventHandler(this.BtnSearchCarreraClick);
			// 
			// txtSearchCarrera
			// 
			this.txtSearchCarrera.Location = new System.Drawing.Point(168, 364);
			this.txtSearchCarrera.Name = "txtSearchCarrera";
			this.txtSearchCarrera.Size = new System.Drawing.Size(120, 20);
			this.txtSearchCarrera.TabIndex = 11;
			// 
			// txtNombreCarrera
			// 
			this.txtNombreCarrera.Location = new System.Drawing.Point(285, 41);
			this.txtNombreCarrera.Name = "txtNombreCarrera";
			this.txtNombreCarrera.Size = new System.Drawing.Size(305, 20);
			this.txtNombreCarrera.TabIndex = 3;
			// 
			// txtAreaCarrera
			// 
			this.txtAreaCarrera.Location = new System.Drawing.Point(285, 79);
			this.txtAreaCarrera.Name = "txtAreaCarrera";
			this.txtAreaCarrera.Size = new System.Drawing.Size(305, 20);
			this.txtAreaCarrera.TabIndex = 4;
			// 
			// txtNivelCarrera
			// 
			this.txtNivelCarrera.Location = new System.Drawing.Point(285, 120);
			this.txtNivelCarrera.Name = "txtNivelCarrera";
			this.txtNivelCarrera.Size = new System.Drawing.Size(305, 20);
			this.txtNivelCarrera.TabIndex = 5;
			// 
			// lblAreaAddCarrera
			// 
			this.lblAreaAddCarrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAreaAddCarrera.ForeColor = System.Drawing.Color.Black;
			this.lblAreaAddCarrera.Location = new System.Drawing.Point(216, 85);
			this.lblAreaAddCarrera.Name = "lblAreaAddCarrera";
			this.lblAreaAddCarrera.Size = new System.Drawing.Size(63, 23);
			this.lblAreaAddCarrera.TabIndex = 1;
			this.lblAreaAddCarrera.Text = "Area";
			// 
			// lblSearchCarrera
			// 
			this.lblSearchCarrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSearchCarrera.ForeColor = System.Drawing.Color.Black;
			this.lblSearchCarrera.Location = new System.Drawing.Point(34, 365);
			this.lblSearchCarrera.Name = "lblSearchCarrera";
			this.lblSearchCarrera.Size = new System.Drawing.Size(119, 23);
			this.lblSearchCarrera.TabIndex = 9;
			this.lblSearchCarrera.Text = "Buscar por codigo:";
			// 
			// tablaCarreras
			// 
			this.tablaCarreras.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaCarreras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaCarreras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Codigo,
			this.Nombre,
			this.Nivel,
			this.Area});
			this.tablaCarreras.Location = new System.Drawing.Point(216, 225);
			this.tablaCarreras.Name = "tablaCarreras";
			this.tablaCarreras.Size = new System.Drawing.Size(449, 102);
			this.tablaCarreras.TabIndex = 7;
			// 
			// Codigo
			// 
			this.Codigo.HeaderText = "Codigo";
			this.Codigo.Name = "Codigo";
			// 
			// Nombre
			// 
			this.Nombre.HeaderText = "Nombre";
			this.Nombre.Name = "Nombre";
			// 
			// Nivel
			// 
			this.Nivel.HeaderText = "Nivel";
			this.Nivel.Name = "Nivel";
			// 
			// Area
			// 
			this.Area.HeaderText = "Area";
			this.Area.Name = "Area";
			// 
			// btnViewCarrera
			// 
			this.btnViewCarrera.Location = new System.Drawing.Point(562, 175);
			this.btnViewCarrera.Name = "btnViewCarrera";
			this.btnViewCarrera.Size = new System.Drawing.Size(81, 23);
			this.btnViewCarrera.TabIndex = 8;
			this.btnViewCarrera.Text = "Mostrar";
			this.btnViewCarrera.UseVisualStyleBackColor = true;
			this.btnViewCarrera.Click += new System.EventHandler(this.BtnViewCarreraClick);
			// 
			// lblNivelAddCarrera
			// 
			this.lblNivelAddCarrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblNivelAddCarrera.ForeColor = System.Drawing.Color.Black;
			this.lblNivelAddCarrera.Location = new System.Drawing.Point(216, 123);
			this.lblNivelAddCarrera.Name = "lblNivelAddCarrera";
			this.lblNivelAddCarrera.Size = new System.Drawing.Size(63, 23);
			this.lblNivelAddCarrera.TabIndex = 2;
			this.lblNivelAddCarrera.Text = "Nivel";
			// 
			// btnAddCarrera
			// 
			this.btnAddCarrera.Location = new System.Drawing.Point(285, 175);
			this.btnAddCarrera.Name = "btnAddCarrera";
			this.btnAddCarrera.Size = new System.Drawing.Size(75, 23);
			this.btnAddCarrera.TabIndex = 6;
			this.btnAddCarrera.Text = "agregar";
			this.btnAddCarrera.UseVisualStyleBackColor = true;
			this.btnAddCarrera.Click += new System.EventHandler(this.BtnAddCarreraClick);
			// 
			// tabMenu
			// 
			this.tabMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tabMenu.Controls.Add(this.tabCarrera);
			this.tabMenu.Controls.Add(this.tabAlumno);
			this.tabMenu.Controls.Add(this.tabAsignatura);
			this.tabMenu.Controls.Add(this.tabGrupo);
			this.tabMenu.Controls.Add(this.tabProfesor);
			this.tabMenu.Controls.Add(this.tabPeriodo);
			this.tabMenu.Controls.Add(this.tabClases);
			this.tabMenu.Controls.Add(this.tabUsuarios);
			this.tabMenu.Location = new System.Drawing.Point(1, 1);
			this.tabMenu.Name = "tabMenu";
			this.tabMenu.SelectedIndex = 0;
			this.tabMenu.Size = new System.Drawing.Size(878, 463);
			this.tabMenu.TabIndex = 0;
			// 
			// tabGrupo
			// 
			this.tabGrupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabGrupo.BackgroundImage")));
			this.tabGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabGrupo.Controls.Add(this.btnActualizarGrupo);
			this.tabGrupo.Controls.Add(this.btnEliminarProducto);
			this.tabGrupo.Controls.Add(this.btnBuscarGrupo);
			this.tabGrupo.Controls.Add(this.txtBuscarGrupo);
			this.tabGrupo.Controls.Add(this.label1);
			this.tabGrupo.Controls.Add(this.tablaGrupo);
			this.tabGrupo.Controls.Add(this.btnMostrarGrupo);
			this.tabGrupo.Controls.Add(this.btnAddGrupo);
			this.tabGrupo.Controls.Add(this.txtNombreGrupo);
			this.tabGrupo.Controls.Add(this.txtCodigoGrupo);
			this.tabGrupo.Controls.Add(this.boxCarreraGrupo);
			this.tabGrupo.Controls.Add(this.lblCarreraGrupo);
			this.tabGrupo.Controls.Add(this.lblNombreGrupo);
			this.tabGrupo.Controls.Add(this.lblCodigoGrupo);
			this.tabGrupo.Location = new System.Drawing.Point(4, 22);
			this.tabGrupo.Name = "tabGrupo";
			this.tabGrupo.Padding = new System.Windows.Forms.Padding(3);
			this.tabGrupo.Size = new System.Drawing.Size(870, 437);
			this.tabGrupo.TabIndex = 4;
			this.tabGrupo.Text = "Grupo";
			this.tabGrupo.UseVisualStyleBackColor = true;
			// 
			// btnActualizarGrupo
			// 
			this.btnActualizarGrupo.Location = new System.Drawing.Point(277, 364);
			this.btnActualizarGrupo.Name = "btnActualizarGrupo";
			this.btnActualizarGrupo.Size = new System.Drawing.Size(75, 23);
			this.btnActualizarGrupo.TabIndex = 15;
			this.btnActualizarGrupo.Text = "Actualizar";
			this.btnActualizarGrupo.UseVisualStyleBackColor = true;
			this.btnActualizarGrupo.Click += new System.EventHandler(this.BtnActualizarGrupoClick);
			// 
			// btnEliminarProducto
			// 
			this.btnEliminarProducto.Location = new System.Drawing.Point(167, 364);
			this.btnEliminarProducto.Name = "btnEliminarProducto";
			this.btnEliminarProducto.Size = new System.Drawing.Size(75, 23);
			this.btnEliminarProducto.TabIndex = 14;
			this.btnEliminarProducto.Text = "Eliminar";
			this.btnEliminarProducto.UseVisualStyleBackColor = true;
			this.btnEliminarProducto.Click += new System.EventHandler(this.BtnEliminarProductoClick);
			// 
			// btnBuscarGrupo
			// 
			this.btnBuscarGrupo.Location = new System.Drawing.Point(51, 364);
			this.btnBuscarGrupo.Name = "btnBuscarGrupo";
			this.btnBuscarGrupo.Size = new System.Drawing.Size(75, 23);
			this.btnBuscarGrupo.TabIndex = 13;
			this.btnBuscarGrupo.Text = "Buscar";
			this.btnBuscarGrupo.UseVisualStyleBackColor = true;
			this.btnBuscarGrupo.Click += new System.EventHandler(this.BtnBuscarGrupoClick);
			// 
			// txtBuscarGrupo
			// 
			this.txtBuscarGrupo.Location = new System.Drawing.Point(156, 306);
			this.txtBuscarGrupo.Name = "txtBuscarGrupo";
			this.txtBuscarGrupo.Size = new System.Drawing.Size(196, 20);
			this.txtBuscarGrupo.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(51, 309);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 11;
			this.label1.Text = "Codigo del grupo";
			// 
			// tablaGrupo
			// 
			this.tablaGrupo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tablaGrupo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaGrupo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaGrupo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.idGrupo,
			this.nombreGrupo,
			this.carreraGrupo,
			this.gradoGrupo});
			this.tablaGrupo.Location = new System.Drawing.Point(371, 55);
			this.tablaGrupo.Name = "tablaGrupo";
			this.tablaGrupo.Size = new System.Drawing.Size(439, 150);
			this.tablaGrupo.TabIndex = 10;
			// 
			// idGrupo
			// 
			this.idGrupo.HeaderText = "ID";
			this.idGrupo.Name = "idGrupo";
			// 
			// nombreGrupo
			// 
			this.nombreGrupo.HeaderText = "Nombre del grupo";
			this.nombreGrupo.Name = "nombreGrupo";
			// 
			// carreraGrupo
			// 
			this.carreraGrupo.HeaderText = "Carrera";
			this.carreraGrupo.Name = "carreraGrupo";
			// 
			// gradoGrupo
			// 
			this.gradoGrupo.HeaderText = "Grado";
			this.gradoGrupo.Name = "gradoGrupo";
			// 
			// btnMostrarGrupo
			// 
			this.btnMostrarGrupo.Location = new System.Drawing.Point(156, 254);
			this.btnMostrarGrupo.Name = "btnMostrarGrupo";
			this.btnMostrarGrupo.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarGrupo.TabIndex = 9;
			this.btnMostrarGrupo.Text = "Mostrar";
			this.btnMostrarGrupo.UseVisualStyleBackColor = true;
			this.btnMostrarGrupo.Click += new System.EventHandler(this.BtnMostrarGrupoClick);
			// 
			// btnAddGrupo
			// 
			this.btnAddGrupo.Location = new System.Drawing.Point(51, 254);
			this.btnAddGrupo.Name = "btnAddGrupo";
			this.btnAddGrupo.Size = new System.Drawing.Size(75, 23);
			this.btnAddGrupo.TabIndex = 8;
			this.btnAddGrupo.Text = "Agregar";
			this.btnAddGrupo.UseVisualStyleBackColor = true;
			this.btnAddGrupo.Click += new System.EventHandler(this.BtnAddGrupoClick);
			// 
			// txtNombreGrupo
			// 
			this.txtNombreGrupo.Location = new System.Drawing.Point(185, 102);
			this.txtNombreGrupo.Name = "txtNombreGrupo";
			this.txtNombreGrupo.Size = new System.Drawing.Size(167, 20);
			this.txtNombreGrupo.TabIndex = 6;
			// 
			// txtCodigoGrupo
			// 
			this.txtCodigoGrupo.Location = new System.Drawing.Point(185, 66);
			this.txtCodigoGrupo.Name = "txtCodigoGrupo";
			this.txtCodigoGrupo.Size = new System.Drawing.Size(167, 20);
			this.txtCodigoGrupo.TabIndex = 5;
			// 
			// boxCarreraGrupo
			// 
			this.boxCarreraGrupo.FormattingEnabled = true;
			this.boxCarreraGrupo.Location = new System.Drawing.Point(185, 141);
			this.boxCarreraGrupo.Name = "boxCarreraGrupo";
			this.boxCarreraGrupo.Size = new System.Drawing.Size(167, 21);
			this.boxCarreraGrupo.TabIndex = 4;
			// 
			// lblCarreraGrupo
			// 
			this.lblCarreraGrupo.Location = new System.Drawing.Point(41, 144);
			this.lblCarreraGrupo.Name = "lblCarreraGrupo";
			this.lblCarreraGrupo.Size = new System.Drawing.Size(100, 23);
			this.lblCarreraGrupo.TabIndex = 2;
			this.lblCarreraGrupo.Text = "Carrera";
			// 
			// lblNombreGrupo
			// 
			this.lblNombreGrupo.Location = new System.Drawing.Point(41, 105);
			this.lblNombreGrupo.Name = "lblNombreGrupo";
			this.lblNombreGrupo.Size = new System.Drawing.Size(100, 23);
			this.lblNombreGrupo.TabIndex = 1;
			this.lblNombreGrupo.Text = "Nombre del grupo";
			// 
			// lblCodigoGrupo
			// 
			this.lblCodigoGrupo.Location = new System.Drawing.Point(41, 68);
			this.lblCodigoGrupo.Name = "lblCodigoGrupo";
			this.lblCodigoGrupo.Size = new System.Drawing.Size(100, 23);
			this.lblCodigoGrupo.TabIndex = 0;
			this.lblCodigoGrupo.Text = "Codigo";
			// 
			// tabClases
			// 
			this.tabClases.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabClases.BackgroundImage")));
			this.tabClases.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabClases.Controls.Add(this.btnBuscarClase);
			this.tabClases.Controls.Add(this.btnActualizar);
			this.tabClases.Controls.Add(this.txtBuscarClase);
			this.tabClases.Controls.Add(this.lblEliminarClase);
			this.tabClases.Controls.Add(this.btnEliminarClase);
			this.tabClases.Controls.Add(this.tablaClase);
			this.tabClases.Controls.Add(this.btnMostrarClases);
			this.tabClases.Controls.Add(this.btnAddClases);
			this.tabClases.Controls.Add(this.boxAsignaturaClases);
			this.tabClases.Controls.Add(this.boxProfesorClases);
			this.tabClases.Controls.Add(this.lblAsignaturaClases);
			this.tabClases.Controls.Add(this.lblProfesorClases);
			this.tabClases.Controls.Add(this.lblGrupoClases);
			this.tabClases.Controls.Add(this.boxGrupoClases);
			this.tabClases.Location = new System.Drawing.Point(4, 22);
			this.tabClases.Name = "tabClases";
			this.tabClases.Padding = new System.Windows.Forms.Padding(3);
			this.tabClases.Size = new System.Drawing.Size(870, 437);
			this.tabClases.TabIndex = 7;
			this.tabClases.Text = "Clases";
			this.tabClases.UseVisualStyleBackColor = true;
			// 
			// btnBuscarClase
			// 
			this.btnBuscarClase.Location = new System.Drawing.Point(342, 340);
			this.btnBuscarClase.Name = "btnBuscarClase";
			this.btnBuscarClase.Size = new System.Drawing.Size(75, 23);
			this.btnBuscarClase.TabIndex = 42;
			this.btnBuscarClase.Text = "Buscar";
			this.btnBuscarClase.UseVisualStyleBackColor = true;
			this.btnBuscarClase.Click += new System.EventHandler(this.BtnBuscarClaseClick);
			// 
			// btnActualizar
			// 
			this.btnActualizar.Location = new System.Drawing.Point(342, 311);
			this.btnActualizar.Name = "btnActualizar";
			this.btnActualizar.Size = new System.Drawing.Size(75, 23);
			this.btnActualizar.TabIndex = 41;
			this.btnActualizar.Text = "Actualizar";
			this.btnActualizar.UseVisualStyleBackColor = true;
			this.btnActualizar.Click += new System.EventHandler(this.BtnActualizarClick);
			// 
			// txtBuscarClase
			// 
			this.txtBuscarClase.Location = new System.Drawing.Point(167, 311);
			this.txtBuscarClase.Name = "txtBuscarClase";
			this.txtBuscarClase.Size = new System.Drawing.Size(135, 20);
			this.txtBuscarClase.TabIndex = 40;
			// 
			// lblEliminarClase
			// 
			this.lblEliminarClase.Location = new System.Drawing.Point(78, 314);
			this.lblEliminarClase.Name = "lblEliminarClase";
			this.lblEliminarClase.Size = new System.Drawing.Size(96, 23);
			this.lblEliminarClase.TabIndex = 39;
			this.lblEliminarClase.Text = "Codigo a buscar:";
			// 
			// btnEliminarClase
			// 
			this.btnEliminarClase.Location = new System.Drawing.Point(342, 280);
			this.btnEliminarClase.Name = "btnEliminarClase";
			this.btnEliminarClase.Size = new System.Drawing.Size(75, 23);
			this.btnEliminarClase.TabIndex = 38;
			this.btnEliminarClase.Text = "Eliminar";
			this.btnEliminarClase.UseVisualStyleBackColor = true;
			this.btnEliminarClase.Click += new System.EventHandler(this.BtnEliminarClaseClick);
			// 
			// tablaClase
			// 
			this.tablaClase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tablaClase.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.tablaClase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablaClase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.codigoClase,
			this.profesorClase,
			this.materiaClase,
			this.grupoClase});
			this.tablaClase.Location = new System.Drawing.Point(439, 62);
			this.tablaClase.Name = "tablaClase";
			this.tablaClase.Size = new System.Drawing.Size(431, 150);
			this.tablaClase.TabIndex = 37;
			// 
			// codigoClase
			// 
			this.codigoClase.HeaderText = "Codigo de la clase";
			this.codigoClase.Name = "codigoClase";
			// 
			// profesorClase
			// 
			this.profesorClase.HeaderText = "Profesor asigando";
			this.profesorClase.Name = "profesorClase";
			// 
			// materiaClase
			// 
			this.materiaClase.HeaderText = "Materia";
			this.materiaClase.Name = "materiaClase";
			// 
			// grupoClase
			// 
			this.grupoClase.HeaderText = "Grupo";
			this.grupoClase.Name = "grupoClase";
			// 
			// btnMostrarClases
			// 
			this.btnMostrarClases.Location = new System.Drawing.Point(183, 247);
			this.btnMostrarClases.Name = "btnMostrarClases";
			this.btnMostrarClases.Size = new System.Drawing.Size(75, 23);
			this.btnMostrarClases.TabIndex = 36;
			this.btnMostrarClases.Text = "Mostrar";
			this.btnMostrarClases.UseVisualStyleBackColor = true;
			this.btnMostrarClases.Click += new System.EventHandler(this.BtnMostrarClasesClick);
			// 
			// btnAddClases
			// 
			this.btnAddClases.Location = new System.Drawing.Point(88, 247);
			this.btnAddClases.Name = "btnAddClases";
			this.btnAddClases.Size = new System.Drawing.Size(75, 23);
			this.btnAddClases.TabIndex = 35;
			this.btnAddClases.Text = "Agregar";
			this.btnAddClases.UseVisualStyleBackColor = true;
			this.btnAddClases.Click += new System.EventHandler(this.BtnAddClasesClick);
			// 
			// boxAsignaturaClases
			// 
			this.boxAsignaturaClases.FormattingEnabled = true;
			this.boxAsignaturaClases.Location = new System.Drawing.Point(228, 191);
			this.boxAsignaturaClases.Name = "boxAsignaturaClases";
			this.boxAsignaturaClases.Size = new System.Drawing.Size(189, 21);
			this.boxAsignaturaClases.TabIndex = 34;
			// 
			// boxProfesorClases
			// 
			this.boxProfesorClases.FormattingEnabled = true;
			this.boxProfesorClases.Location = new System.Drawing.Point(228, 142);
			this.boxProfesorClases.Name = "boxProfesorClases";
			this.boxProfesorClases.Size = new System.Drawing.Size(189, 21);
			this.boxProfesorClases.TabIndex = 33;
			// 
			// lblAsignaturaClases
			// 
			this.lblAsignaturaClases.Location = new System.Drawing.Point(88, 194);
			this.lblAsignaturaClases.Name = "lblAsignaturaClases";
			this.lblAsignaturaClases.Size = new System.Drawing.Size(64, 23);
			this.lblAsignaturaClases.TabIndex = 32;
			this.lblAsignaturaClases.Text = "Asignatura:";
			// 
			// lblProfesorClases
			// 
			this.lblProfesorClases.Location = new System.Drawing.Point(88, 145);
			this.lblProfesorClases.Name = "lblProfesorClases";
			this.lblProfesorClases.Size = new System.Drawing.Size(98, 23);
			this.lblProfesorClases.TabIndex = 31;
			this.lblProfesorClases.Text = "Profesor Asignado:";
			// 
			// lblGrupoClases
			// 
			this.lblGrupoClases.Location = new System.Drawing.Point(88, 93);
			this.lblGrupoClases.Name = "lblGrupoClases";
			this.lblGrupoClases.Size = new System.Drawing.Size(64, 23);
			this.lblGrupoClases.TabIndex = 30;
			this.lblGrupoClases.Text = "Grupo:";
			// 
			// boxGrupoClases
			// 
			this.boxGrupoClases.FormattingEnabled = true;
			this.boxGrupoClases.Location = new System.Drawing.Point(228, 90);
			this.boxGrupoClases.Name = "boxGrupoClases";
			this.boxGrupoClases.Size = new System.Drawing.Size(189, 21);
			this.boxGrupoClases.TabIndex = 29;
			// 
			// tabUsuarios
			// 
			this.tabUsuarios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabUsuarios.BackgroundImage")));
			this.tabUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.tabUsuarios.Controls.Add(this.label2);
			this.tabUsuarios.Controls.Add(this.btnAddUsuarioE);
			this.tabUsuarios.Controls.Add(this.btnAddUsuarioP);
			this.tabUsuarios.Controls.Add(this.boxEstudianteUsuario);
			this.tabUsuarios.Controls.Add(this.lblConfirmarContraseña);
			this.tabUsuarios.Controls.Add(this.txtConfContrasena);
			this.tabUsuarios.Controls.Add(this.boxMaestroUsuario);
			this.tabUsuarios.Controls.Add(this.label6);
			this.tabUsuarios.Controls.Add(this.lblCotraseña);
			this.tabUsuarios.Controls.Add(this.txtContrasena);
			this.tabUsuarios.Controls.Add(this.lblMaestro);
			this.tabUsuarios.Location = new System.Drawing.Point(4, 22);
			this.tabUsuarios.Name = "tabUsuarios";
			this.tabUsuarios.Padding = new System.Windows.Forms.Padding(3);
			this.tabUsuarios.Size = new System.Drawing.Size(870, 437);
			this.tabUsuarios.TabIndex = 8;
			this.tabUsuarios.Text = "Usuarios";
			this.tabUsuarios.UseVisualStyleBackColor = true;
			// 
			// btnAddUsuarioE
			// 
			this.btnAddUsuarioE.Location = new System.Drawing.Point(585, 181);
			this.btnAddUsuarioE.Name = "btnAddUsuarioE";
			this.btnAddUsuarioE.Size = new System.Drawing.Size(110, 23);
			this.btnAddUsuarioE.TabIndex = 11;
			this.btnAddUsuarioE.Text = "Agregar Estudiante";
			this.btnAddUsuarioE.UseVisualStyleBackColor = true;
			this.btnAddUsuarioE.Click += new System.EventHandler(this.BtnAddUsuarioEClick);
			// 
			// btnAddUsuarioP
			// 
			this.btnAddUsuarioP.Location = new System.Drawing.Point(200, 186);
			this.btnAddUsuarioP.Name = "btnAddUsuarioP";
			this.btnAddUsuarioP.Size = new System.Drawing.Size(102, 23);
			this.btnAddUsuarioP.TabIndex = 10;
			this.btnAddUsuarioP.Text = "Agregar Profesor";
			this.btnAddUsuarioP.UseVisualStyleBackColor = true;
			this.btnAddUsuarioP.Click += new System.EventHandler(this.BtnAddUsuarioPClick);
			// 
			// boxEstudianteUsuario
			// 
			this.boxEstudianteUsuario.FormattingEnabled = true;
			this.boxEstudianteUsuario.Location = new System.Drawing.Point(550, 98);
			this.boxEstudianteUsuario.Name = "boxEstudianteUsuario";
			this.boxEstudianteUsuario.Size = new System.Drawing.Size(164, 21);
			this.boxEstudianteUsuario.TabIndex = 9;
			// 
			// lblConfirmarContraseña
			// 
			this.lblConfirmarContraseña.Location = new System.Drawing.Point(404, 186);
			this.lblConfirmarContraseña.Name = "lblConfirmarContraseña";
			this.lblConfirmarContraseña.Size = new System.Drawing.Size(111, 23);
			this.lblConfirmarContraseña.TabIndex = 8;
			this.lblConfirmarContraseña.Text = "Confirmar contraseña";
			// 
			// txtConfContrasena
			// 
			this.txtConfContrasena.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.txtConfContrasena.Location = new System.Drawing.Point(375, 209);
			this.txtConfContrasena.Name = "txtConfContrasena";
			this.txtConfContrasena.Size = new System.Drawing.Size(164, 20);
			this.txtConfContrasena.TabIndex = 7;
			this.txtConfContrasena.UseSystemPasswordChar = true;
			// 
			// boxMaestroUsuario
			// 
			this.boxMaestroUsuario.FormattingEnabled = true;
			this.boxMaestroUsuario.Location = new System.Drawing.Point(188, 98);
			this.boxMaestroUsuario.Name = "boxMaestroUsuario";
			this.boxMaestroUsuario.Size = new System.Drawing.Size(164, 21);
			this.boxMaestroUsuario.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(106, 129);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(70, 23);
			this.label6.TabIndex = 5;
			// 
			// lblCotraseña
			// 
			this.lblCotraseña.Location = new System.Drawing.Point(428, 135);
			this.lblCotraseña.Name = "lblCotraseña";
			this.lblCotraseña.Size = new System.Drawing.Size(70, 23);
			this.lblCotraseña.TabIndex = 4;
			this.lblCotraseña.Text = "Contraseña";
			// 
			// txtContrasena
			// 
			this.txtContrasena.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.txtContrasena.Location = new System.Drawing.Point(375, 161);
			this.txtContrasena.Name = "txtContrasena";
			this.txtContrasena.Size = new System.Drawing.Size(164, 20);
			this.txtContrasena.TabIndex = 2;
			this.txtContrasena.UseSystemPasswordChar = true;
			// 
			// lblMaestro
			// 
			this.lblMaestro.Location = new System.Drawing.Point(246, 72);
			this.lblMaestro.Name = "lblMaestro";
			this.lblMaestro.Size = new System.Drawing.Size(70, 23);
			this.lblMaestro.TabIndex = 1;
			this.lblMaestro.Text = "Maestro";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(604, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 23);
			this.label2.TabIndex = 12;
			this.label2.Text = "Estudiante";
			// 
			// Menu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(891, 476);
			this.Controls.Add(this.tabMenu);
			this.Name = "Menu";
			this.Text = "Menu";
			this.Load += new System.EventHandler(this.MenuLoad);
			this.tabPeriodo.ResumeLayout(false);
			this.tabPeriodo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaPeriodos)).EndInit();
			this.tabProfesor.ResumeLayout(false);
			this.tabProfesor.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaProfesores)).EndInit();
			this.tabAsignatura.ResumeLayout(false);
			this.tabAsignatura.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaAsignatura)).EndInit();
			this.tabAlumno.ResumeLayout(false);
			this.tabAlumno.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAlumno)).EndInit();
			this.tabCarrera.ResumeLayout(false);
			this.tabCarrera.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaCarreras)).EndInit();
			this.tabMenu.ResumeLayout(false);
			this.tabGrupo.ResumeLayout(false);
			this.tabGrupo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaGrupo)).EndInit();
			this.tabClases.ResumeLayout(false);
			this.tabClases.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tablaClase)).EndInit();
			this.tabUsuarios.ResumeLayout(false);
			this.tabUsuarios.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
