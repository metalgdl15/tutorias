﻿/*
 * Created by SharpDevelop.
 * User: Adan
 * Date: 01/12/2018
 * Time: 13:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace Tutorias
{
	/// <summary>
	/// Description of menuProfesor.
	/// </summary>
	public partial class menuProfesor : Form
	{
		public Usuario User {
			get {
				return user;
			}
			set {
				user = value;
			}
		}

		
		private Usuario user = new Usuario ();
		public menuProfesor(Usuario us)
		{
			User = us;
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		//Conexion a la db
		MySqlConnection Conectar (){
			MySqlConnection conexion = new MySqlConnection();
			
			conexion.ConnectionString="Server=localhost; password=; database=tutoria; User Id=root;";
			
			return conexion;
		}
		
		//Inicializar lo necesario
		void MenuProfesorLoad(object sender, EventArgs e)
		{
			agregarPeriodos();
			agregarAlumnos();
			agreaUsuario();
		}
		
		public void agreaUsuario(){
			MySqlConnection conexion = Conectar();
			
			try {
				String query = "SELECT CONCAT(aPaterno,' ',aMaterno, ' ', nombre) nom FROM profesor WHERE Codigo="+User.Codigo;
				MySqlCommand comando = new MySqlCommand(query, conexion);
				
				conexion.Open();
				MySqlDataReader rd = comando.ExecuteReader();
				
				while(rd.Read()){
					lblUsuario.Text = Convert.ToString(rd["nom"]);
				}
				conexion.Close();
				
			} catch (MySqlException) {
				
				MessageBox.Show("error");
			}
		}
				
		//Inicializa Periodos
		public void agregarPeriodos(){
			
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT idPeriodo, CONCAT(inicio,'   <->   ',fin) as ciclo FROM periodo";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxPeriodoIndividual.ValueMember ="idPeriodo";
				boxPeriodoIndividual.DisplayMember="ciclo";
				boxPeriodoIndividual.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		//inicializa alumnos
		public void agregarAlumnos(){
			boxAlumnoIndividual.DataSource=null;
			MySqlConnection conexion = Conectar();
			
			try {
				conexion.Open();
				String query = "SELECT Codigo, CONCAT(apellidoP,' ',apellidoP,' ',nombre) as alumno FROM estudiante";
				MySqlCommand comando = new MySqlCommand(query, conexion);
			
				
				MySqlDataAdapter da2 = new MySqlDataAdapter(comando);
				DataTable dt2 = new DataTable();
				
				dt2.Rows.InsertAt(dt2.NewRow(), 0);
				
				da2.Fill(dt2);
				
				boxAlumnoIndividual.ValueMember ="Codigo";
				boxAlumnoIndividual.DisplayMember="alumno";
				boxAlumnoIndividual.DataSource=dt2;
				
				conexion.Close();
			} catch (MySqlException) {
				MessageBox.Show("algo anda mal");
			}
		}
		
		
		void BtnConsultarClaseClick(object sender, EventArgs e)
		{
			tablaClaseProfesor.Rows.Clear();
				
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "SELECT pa.id, CONCAT(p.aPaterno,' ',p.aMaterno,' ',p.nombre) as profe, a.nombre as materia, CONCAT(gru.Nombre,' ', gra.Nombre) as gru " +
						"FROM (profesor_asignatura pa, grupo_grado g) " +
						"INNER JOIN profesor p ON p.Codigo = pa.idProfesor " +
						"INNER JOIN asignatura a ON a.idAsignatura = pa.idAsignatura " +
						"INNER JOIN grupo gru ON g.idGrupo = gru.idGrupo AND pa.idGrupo_grado = g.id " +
						"INNER JOIN grado gra ON gra.idGrado = g.idGrado WHERE p.Codigo="+User.Codigo;
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
				
					while (rd.Read()) {
						int renglon = tablaClaseProfesor.Rows.Add();
						
						tablaClaseProfesor.Rows[renglon].Cells["codigoClase"].Value=
							rd.GetInt32(rd.GetOrdinal("id")).ToString();
						tablaClaseProfesor.Rows[renglon].Cells["profesorClase"].Value=
							rd.GetString(rd.GetOrdinal("profe"));
						tablaClaseProfesor.Rows[renglon].Cells["materiaClase"].Value=
							rd.GetString(rd.GetOrdinal("materia"));
						tablaClaseProfesor.Rows[renglon].Cells["grupoClase"].Value=
							rd.GetString(rd.GetOrdinal("gru"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
			
		}
		
		//Agregar tutoria individual
		void BtnAgregearTutoriaIndClick(object sender, EventArgs e)
		{
			String alumno = boxAlumnoIndividual.SelectedValue.ToString();
			String periodo = boxPeriodoIndividual.SelectedValue.ToString();
			
			if (alumno == "" || periodo == ""){
				MessageBox.Show("Ingrese todos los campos");
			}else{
				MySqlConnection conexion = Conectar();
				
				try {
					String query =" INSERT INTO tutoria_individual (idProfesor,idEstudiante, idPeriodo) VALUES("+User.Codigo+","+alumno+","+periodo+")";
					MySqlCommand comando = new MySqlCommand (query,conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha agregado");
				} catch (MySqlException) {
					
					MessageBox.Show("ERROR");
				}
			}
		}
		
		//Consutal tutoria Individual
		void BtnMostrarTutoriaClick(object sender, EventArgs e)
		{
			tablaTutoria.Rows.Clear();
			
				MySqlConnection conexion = Conectar();
				try {
					String query = "SELECT t.idTutoria, CONCAT(pr.aPaterno,' ',pr.aMaterno,' ',pr.nombre ) as profe, pe.nombre as perio, CONCAT(e.apellidoP,' ',e.apellidoM,' ' ,e.nombre) as alumno " +
						"FROM tutoria_individual t " +
						"INNER JOIN profesor pr ON t.idProfesor = pr.Codigo " +
						"INNER JOIN estudiante e ON t.idEstudiante = e.Codigo " +
						"INNER JOIN periodo pe ON t.idPeriodo = pe.idPeriodo WHERE t.idProfesor="+User.Codigo;
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
					
					while (rd.Read()) {
						int renglon = tablaTutoria.Rows.Add();
						
						tablaTutoria.Rows[renglon].Cells["codigoTutoria"].Value=
							rd.GetInt32(rd.GetOrdinal("idTutoria")).ToString();
						tablaTutoria.Rows[renglon].Cells["Tutor"].Value=
							rd.GetString(rd.GetOrdinal("profe"));
						tablaTutoria.Rows[renglon].Cells["Periodo"].Value=
							rd.GetString(rd.GetOrdinal("perio"));
						tablaTutoria.Rows[renglon].Cells["Estudiante"].Value=
							rd.GetString(rd.GetOrdinal("alumno"));
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
		}
		
		//Insertar bitacora
		void BtnAddBitacoraClick(object sender, EventArgs e)
		{	
			String objetivo = txtObjetivo.Text;
			String meta = txtMeta.Text;
			String acuerdo= txtAcuerdo.Text;
			
			String id = txtTutoriaBusqueda.Text;
						
				MySqlConnection conexion = Conectar();
				
				try {
					String query= "INSERT INTO bitacora (objetivo,meta,acuerdo,idTutoria) VALUES ('"+objetivo+"','"+meta+"','"+acuerdo+"',"+id+")";
					MySqlCommand comando = new MySqlCommand(query,conexion);
					
					conexion.Open();
					comando.ExecuteNonQuery();
					conexion.Close();
					MessageBox.Show("Se ha añadido la bitacora");
				} catch (Exception) {
					
					MessageBox.Show("Error");
				}
			
		}
		
		//Mostrar bitacora
		void MostrarClick(object sender, EventArgs e)
		{	
			String id = txtTutoriaBusqueda.Text;
			txtObjetivo.Clear();
			txtMeta.Clear();
			txtAcuerdo.Clear();
			
				MySqlConnection conexion = Conectar();
				try {
					String query = "SELECT b.objetivo, b.acuerdo, b.meta FROM tutoria_individual t " +
						"INNER JOIN bitacora b ON b.idTutoria = t.idTutoria " +
						"WHERE b.idTutoria="+id+" AND t.idProfesor="+User.Codigo;
						
					MySqlCommand comando =new MySqlCommand (query, conexion);
					
					conexion.Open();
					MySqlDataReader rd = comando.ExecuteReader();
					
					while (rd.Read()) {						
						
						txtObjetivo.Text= Convert.ToString(rd["objetivo"]);
						txtMeta.Text= Convert.ToString(rd["meta"]);
						txtAcuerdo.Text = Convert.ToString(rd["acuerdo"]);
					}
					
					conexion.Close();
				} catch (MySqlException) {
					MessageBox.Show("error");
				}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			String objetivo = txtObjetivo.Text;
			String meta = txtMeta.Text;
			String acuerdo = txtAcuerdo.Text;
			String idBitacora = txtTutoriaBusqueda.Text;
			
			if (objetivo == "" || meta == "" || acuerdo == ""){
				MessageBox.Show("Llene los 3 cadros de texto");
			}else{
				MySqlConnection conexion = Conectar();
				
				try {
					String query = "UPDATE bitacora SET objetivo='"+objetivo+"', meta='"+meta+"', acuerdo='"+acuerdo+"' WHERE idTutoria="+idBitacora;
					MySqlCommand comando = new MySqlCommand(query, conexion);
					conexion.Open();
					
					comando.ExecuteNonQuery();
					
					conexion.Close();
					
					MessageBox.Show("Se ha actualizado");
				} catch (MySqlException) {
					
					MessageBox.Show("Error");
				}
			}
		}
		
	}
}
